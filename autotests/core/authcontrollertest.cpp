// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "authcontroller.h"
#include "fake/fakenetworkdispatcher.h"
#include "utils/stringliterals.h"
#include "utils/memory.h"

#include <QObject>
#include <QTest>
#include <QJsonObject>
#include <QDesktopServices>
#include <QSignalSpy>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <qnamespace.h>

using namespace Shirk::Core;
using namespace Shirk::StringLiterals;

class FakeBrowser : public QObject
{
    Q_OBJECT

public:
    explicit FakeBrowser()
        : QObject() {
        QDesktopServices::setUrlHandler(u"https"_qs, this, "openBrowser");
        QDesktopServices::setUrlHandler(u"http"_qs, this, "openBrowser");
    }

public Q_SLOTS:
    void openBrowser(const QUrl &url) {
        QUrlQuery query{url};
        mRedirectUri = query.queryItemValue(u"redirect_uri"_qs);
        mState = query.queryItemValue(u"state"_qs);
        Q_EMIT browserOpened(url);
    }

    void sendResponse(const QString &code) {
        QUrl uri{mRedirectUri};
        QUrlQuery query{uri};
        query.addQueryItem(u"code"_qs, code);
        query.addQueryItem(u"state"_qs, mState);
        uri.setQuery(query);
        auto nam = Shirk::make_unique_qobject<QNetworkAccessManager>();
        auto reply_ = nam->get(QNetworkRequest{uri});
        auto reply = Shirk::UniqueQObjectPtr<QNetworkReply>(reply_);
        connect(reply_, &QNetworkReply::finished,
                [nam = std::move(nam), reply = std::move(reply)]() mutable {
                    nam.reset();
                    reply.reset();
                });
    }

Q_SIGNALS:
    void browserOpened(const QUrl &url);

private:
    QUrl mRedirectUri;
    QString mState;
};


class AuthControllerTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void testAuthentication() {
        FakeBrowser fakeBrowser;
        Test::FakeNetworkDispatcher fakeNetworkDispatcher;
        QSignalSpy browserSpy(&fakeBrowser, &FakeBrowser::browserOpened);
        QVERIFY(browserSpy.isValid());

        AuthController controller;
        QCOMPARE(controller.state(), AuthController::State::None);
        controller.setNetworkDispatcherOverride(fakeNetworkDispatcher);
        bool futureTriggered = false;
        controller.start().then([&futureTriggered](std::unique_ptr<Team> team) mutable {
            futureTriggered = true;

            QCOMPARE(team->id(), u"T123456"_qs);
            QCOMPARE(team->accessToken(), u"xoxp-123456"_qs);
            QCOMPARE(team->botAccessToken(), u"xoxb-123456"_qs);
            QCOMPARE(team->name(), u"Test"_qs);
        });

        // The controller tries to open a browser and expects a temporary code returned
        // via GET method on redirect_uri
        {
            QTRY_COMPARE(browserSpy.size(), 1);
            QCOMPARE(controller.state(), AuthController::State::WaitingForBrowser);
            const auto url = browserSpy[0][0].toUrl();
            QCOMPARE(url.scheme(), u"https"_qs);
            QCOMPARE(url.host(), u"slack.com"_qs);
            QCOMPARE(url.path(), u"/oauth/authorize"_qs);
            QVERIFY(QUrlQuery(url).hasQueryItem(u"redirect_uri"_qs));
            fakeBrowser.sendResponse(u"123456"_qs);
        }

        // Next the controller tries to exchange the temporary code for a full
        // OAuth token
        {
            QTRY_VERIFY(fakeNetworkDispatcher.lastRequest().isValid());
            QCOMPARE(controller.state(), AuthController::State::RetrievingToken);
            const auto url = fakeNetworkDispatcher.lastRequest();
            QCOMPARE(url.path(), u"/api/oauth.access"_qs);
            QCOMPARE(QUrlQuery(url).queryItemValue(u"code"_qs), u"123456"_qs);
            fakeNetworkDispatcher.setResponse({
                {u"ok"_qs, true},
                {u"access_token"_qs, u"xoxp-123456"_qs},
                {u"scope"_qs, u"identify,bot,channels:history,groups:history,im:history,mpim:history,"
                               "channels:read,emoji:read,files:read,groups:read,im:read,mpim:read,"
                               "reactions:read,reminders:read,search:read,stars:read,team:read,users:read,"
                               "users:read.email,pins:read,usergroups:read,dnd:read,users.profile:read,"
                               "channels:write,chat:write:user,files:write:user,groups:write,im:write,"
                               "mpim:write,reactions:write,reminders:write,stars:write,users:write,"
                               "pins:write,usergroups:write,dnd:write,users.profile:write,links:read,"
                               "links:write,remote_files:write,remote_files:share,remote_files:read"_qs},
                {u"user_id"_qs, u"U123456"_qs},
                {u"team_id"_qs, u"T123456"_qs},
                {u"enterprise_id"_qs, QJsonValue{} },
                {u"team_name"_qs, u"Test"_qs},
                {u"bot"_qs, QJsonObject{
                    {u"bot_user_id"_qs, u"B123456"_qs},
                    {u"bot_access_token"_qs, u"xoxb-123456"_qs}
                }}
            });
        }

        // Query for team info follows
        {
            QTRY_VERIFY(fakeNetworkDispatcher.lastRequest().isValid());
            QCOMPARE(controller.state(), AuthController::State::RetrievingTeamInfo);
            const auto url = fakeNetworkDispatcher.lastRequest();
            QCOMPARE(url.path(), u"/api/team.info");
            QCOMPARE(QUrlQuery(url).queryItemValue(u"team"_qs), u"T123456"_qs);
            fakeNetworkDispatcher.setResponse({
                    {u"ok"_qs, true},
                    {u"team"_qs, QJsonObject{
                        {u"id"_qs, u"T123456"_qs},
                        {u"name"_qs, u"Test"_qs},
                        {u"domain"_qs, u"test"_qs},
                        {u"email_domain"_qs, u""_qs},
                        {u"icon"_qs, QJsonObject{
                            {u"image_34"_qs, u"https://slack.com/avatar_34.png"_qs},
                            {u"image_44"_qs, u"https://slack.com/avatar_44.png"_qs},
                            {u"image_68"_qs, u"https://slack.com/avatar_68.png"_qs},
                            {u"image_88"_qs, u"https://slack.com/avatar_88.png"_qs},
                            {u"image_102"_qs, u"https://slack.com/avatar_102.png"_qs},
                            {u"image_132"_qs, u"https://slack.com/avatar_132.png"_qs},
                            {u"image_230"_qs, u"https://slack.com/avatar_230.png"_qs},
                            {u"image_original"_qs, u"https://slack.com/avatar_original.png"_qs}
                        }}
                    }}
                });
        }

        QCOMPARE(controller.state(), AuthController::State::Done);

        QTRY_VERIFY(futureTriggered);
    }


};

QTEST_MAIN(AuthControllerTest)

#include "authcontrollertest.moc"
