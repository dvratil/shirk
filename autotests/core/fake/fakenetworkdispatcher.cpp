// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "fakenetworkdispatcher.h"

#include <QDebug>

using namespace Shirk::Core::Test;

QUrl FakeNetworkDispatcher::lastRequest() const
{
    return mLastRequest ? mLastRequest->url : QUrl{};
}

void FakeNetworkDispatcher::setResponse(const QJsonObject &response)
{
    Q_ASSERT(!mLastRequest->promise.isFulfilled());
    mLastRequest->promise.setResult(response);
}

void FakeNetworkDispatcher::dispatchRequest(Request &&request)
{
    Q_ASSERT(!request.promise.isFulfilled());
    mLastRequest = std::make_unique<Request>(std::move(request));
}
