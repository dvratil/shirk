// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "networkdispatcher.h"

namespace Shirk::Core::Test
{

class FakeNetworkDispatcher : public NetworkDispatcher
{
    Q_OBJECT
public:
    using Handler = std::function<QJsonObject(const QUrl &)>;

    QUrl lastRequest() const;
    void setResponse(const QJsonObject &response);

protected:
    void dispatchRequest(Request &&request) override;

private:
    std::unique_ptr<Request> mLastRequest;
};

}
