// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include <QObject>
#include <QTest>
#include <QStringList>

#include "utils/ranges.h"
#include "utils/stringliterals.h"

#include <range/v3/range_fwd.hpp>
#include <range/v3/view/filter.hpp>
#include <type_traits>

using namespace Shirk::StringLiterals;

class RangesTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void test_toCont()
    {
        const QStringList in{u"1"_qs, u"2"_qs, u"3"_qs, u"4"_qs};
        const auto out = in | ranges::views::filter([](const QString &v) { return v.toInt() % 2 == 0; })
                            | ranges::to_qt<QStringList>();
        static_assert(std::is_same_v<decltype(out), const QStringList>);
        QCOMPARE(out, (QStringList{u"2"_qs, u"4"_qs}));
    }

    void test_toContT()
    {
        const QList<int> in{1, 2, 3, 4, 5, 6, 7, 8};
        const auto out = in | ranges::views::filter([](int i) { return i % 2 == 0; })
                            | ranges::to_qt<QList<int>>();
        static_assert(std::is_same_v<decltype(out), const QList<int>>);
        QCOMPARE(out, (QList<int>{2, 4, 6 ,8}));
    }
};

QTEST_GUILESS_MAIN(RangesTest)

#include "rangestest.moc"
