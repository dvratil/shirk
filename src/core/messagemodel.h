// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QAbstractListModel>
#include <QDateTime>
#include <QQmlListProperty>

#include <memory>

#include "slackapi/events.h"
#include "core/rtmeventlistener.h"

namespace Shirk::SlackAPI
{
class Message;
class Reaction;
}
namespace Shirk::SlackAPI::RTM
{
struct MessageEvent;
}

namespace Shirk::Core
{

class TeamController;
class Conversation;
class User;
class UserManager;
class MessageModel;
class Message;

class Reaction final: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(int count READ count CONSTANT)
    Q_PROPERTY(QQmlListProperty<Shirk::Core::User> users READ users CONSTANT)

public:
    Reaction(const SlackAPI::Reaction &reaction, UserManager &userManager);
    Reaction(const SlackAPI::RTM::ReactionAddedEvent &event, UserManager &userManager);

    void update(const SlackAPI::RTM::ReactionAddedEvent &event, UserManager &userManager);

    QString name() const { return mName; }
    int count() const { return mCount; }
    QQmlListProperty<Shirk::Core::User> users();

private:
    QString mName;
    std::vector<std::unique_ptr<User>> mUsers;
    int mCount = 0;
};

// FIXME: This is too heavy for QML
class Message final: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text READ text CONSTANT)
    Q_PROPERTY(Shirk::Core::User *user READ user CONSTANT)
    Q_PROPERTY(QDateTime timestamp READ timestamp CONSTANT)
    Q_PROPERTY(int replyCount READ replyCount CONSTANT)
    Q_PROPERTY(QDateTime latestReply READ latestReply CONSTANT)
    Q_PROPERTY(QQmlListProperty<Shirk::Core::User> replyUsers READ replyUsers CONSTANT)
    Q_PROPERTY(QQmlListProperty<Shirk::Core::Reaction> reactions READ reactions CONSTANT)

public:
    Message(const SlackAPI::Message &msg, UserManager &userManager);
    Message(const SlackAPI::RTM::MessageEvent &msg, UserManager &userManager);

    QDateTime timestamp() const { return mTs.toTimestamp(); }
    QString text() const { return mText; }
    User *user() const { return mUser.get(); }
    int replyCount() const { return mReplyCount; }
    QDateTime latestReply() const { return mLatestReply.has_value() ? mLatestReply->toTimestamp() : QDateTime{}; }
    QQmlListProperty<User> replyUsers();
    QQmlListProperty<Reaction> reactions();

    SlackAPI::TSID ts() const { return mTs; }

    void update(const SlackAPI::RTM::ReactionAddedEvent &event, UserManager &userManager);
private:
    SlackAPI::TSID mTs;
    QString mText;
    std::unique_ptr<User> mUser;
    int mReplyCount = 0;
    std::optional<SlackAPI::TSID> mLatestReply;
    std::vector<std::unique_ptr<User>> mReplyUsers;
    std::vector<std::unique_ptr<Reaction>> mReactions;
};

class MessageModel final: public QAbstractListModel
                        , public Core::RTMEventListener
{
    Q_OBJECT
public:
    enum class Role {
        Message = Qt::UserRole + 1,
        Date
    };

    MessageModel(TeamController &controller, Conversation &conversation);
    ~MessageModel() override = default;

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    Q_INVOKABLE void fetchOlder();

protected:
    void rtmMessageEvent(const SlackAPI::RTM::MessageEvent &event) override;
    void rtmReactionAddedEvent(const SlackAPI::RTM::ReactionAddedEvent &event) override;

private:
    void handleResponse(const QJsonValue &val);

    TeamController &mController;
    Conversation &mConversation;

    std::optional<SlackAPI::TSID> mOldest;

    std::vector<std::unique_ptr<Message>> mMessages;
};

} // namespace
