// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "networkdispatcher.h"
#include "core_debug.h"
#include "utils/stringliterals.h"

#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QUrlQuery>

#include <functional>
#include <qloggingcategory.h>
#include <qssl.h>

using namespace Shirk::Core;
using namespace Shirk::StringLiterals;

Q_LOGGING_CATEGORY(LOG_NETWORKDISPATCHER, "org.kde.shirk.core.networkdispatcher", QtInfoMsg)

NetworkDispatcher::NetworkDispatcher()
{
    mNam.setStrictTransportSecurityEnabled(true);
    mNam.setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
}

NetworkDispatcher::~NetworkDispatcher() = default;

void NetworkDispatcher::setTeam(const Team &team)
{
    mTeam = std::cref(team);
}

Future<QJsonValue> NetworkDispatcher::enqueueRequest(SlackAPI::Method method, const QUrl &url)
{
    auto &request = mPendingRequests.emplace_back(Request{method, url, Promise<QJsonValue>{}});
    auto future = request.promise.getFuture();
    tryDispatchNextRequest();
    return future;
}


void NetworkDispatcher::tryDispatchNextRequest()
{
    if (mRunningRequests.size() >= mMaxRunningRequests || mPendingRequests.empty()) {
        return;
    }

    dispatchRequest(std::move(mPendingRequests.front()));
    mPendingRequests.pop_front();
}

void NetworkDispatcher::dispatchRequest(Request &&data)
{
    QNetworkRequest request(data.url);
    auto ssl = request.sslConfiguration();
    // Starting March, Slack will not support TLS 1.0 and 1.1
    ssl.setProtocol(QSsl::TlsV1_2OrLater);
    request.setSslConfiguration(ssl);

    qCDebug(LOG_NETWORKDISPATCHER) << "Sending request to" << data.url;
    UniqueQObjectPtr<QNetworkReply> reply{mNam.get(request)};
    connect(reply.get(), &QNetworkReply::finished,
            this, [this, reply = reply.get(), request = std::move(data)]() mutable {
                qCDebug(LOG_NETWORKDISPATCHER) << "Received response for" << reply->url();
                const auto rawData = reply->readAll();
                qCDebug(LOG_NETWORKDISPATCHER) << rawData;
                const auto json = QJsonDocument::fromJson(rawData);
                if (!json[QStringLiteral("ok")].toBool()) {
                    request.promise.setError(u"Error response: %1"_qs.arg(QString::fromUtf8(rawData)));
                } else{
                    request.promise.setResult(json.object());
                }

                auto it = std::find_if(mRunningRequests.begin(), mRunningRequests.end(),
                                       [reply](const auto &r) { return r.get() == reply; });
                Q_ASSERT(it != mRunningRequests.end());
                mRunningRequests.erase(it);

                tryDispatchNextRequest();
            });
    mRunningRequests.push_back(std::move(reply));
}

QUrl NetworkDispatcher::urlForEndpoint(QStringView endpoint, const QUrlQuery &query, std::optional<QString> token) const
{
    QUrl url(QStringLiteral("https://slack.com/api/%1").arg(endpoint));
    if (token.has_value()) {
        QUrlQuery q(query);
        q.addQueryItem(QStringLiteral("token"), *token);
        url.setQuery(q);
    } else {
        url.setQuery(query);
    }

    return url;
}
