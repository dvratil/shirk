// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "conversation.h"
#include "usermanager.h"
#include "conversationsmodel.h"
#include "networkdispatcher.h"
#include "rtmcontroller.h"
#include "emojimodel.h"
#include "utils/memory.h"

#include <QObject>

#include <memory>

namespace Shirk::Core {

class Team;
class ConversationsModel;
class RTMController;
class MessageModel;

class TeamController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(Shirk::Core::ConversationsModel *conversationsModel READ conversationsModelPtr CONSTANT)
    Q_PROPERTY(Shirk::Core::Team *team READ teamPtr CONSTANT)
    Q_PROPERTY(Shirk::Core::EmojiModel *emojiModel READ emojiModelPtr CONSTANT)

public:
    enum class Status {
        Connecting,
        Reconnecting,
        Connected,
        Disconnected
    };
    Q_ENUM(Status)

    TeamController(std::unique_ptr<Team> team);
    ~TeamController() override;

    /**
     * Starts to connect to Slack.
     */
    void start();
    /**
     * Disconnects from Slack.
     */
    void quit();

    const Team &team() const;
    Team *teamPtr();
    Team &team();

    UserManager &userManager() {
        return mUserManager;
    }
    ConversationsModel &conversations() {
        return mConversations;
    }
    ConversationsModel *conversationsModelPtr() {
        return &mConversations;
    }

    NetworkDispatcher &networkDispatcher() {
        return mNetworkDispatcher;
    }

    RTMController &rtmController() {
        return mRTMController;
    }

    EmojiModel &emojiModel() {
        return mEmojiModel;
    }

    EmojiModel *emojiModelPtr() {
        return &mEmojiModel;
    }

    Q_INVOKABLE Shirk::Core::MessageModel *messageModelForConversation(Shirk::Core::Conversation *conv);

    Q_INVOKABLE void joinChannel(Shirk::Core::Conversation *conversation);
    Q_INVOKABLE void leaveChannel(Shirk::Core::Conversation *conversation);

    Status status() const;

    void updateConfig(Config &config);

Q_SIGNALS:
    void statusChanged(Status status);


protected:
    void setStatus(Status status);

private:
    std::unique_ptr<Team> mTeam;

    NetworkDispatcher mNetworkDispatcher;
    RTMController mRTMController;
    UserManager mUserManager;
    ConversationsModel mConversations;
    EmojiModel mEmojiModel;
    FutureWatcher mStartWatcher;

    Status mStatus = Status::Disconnected;
};

}
