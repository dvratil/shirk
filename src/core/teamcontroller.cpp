// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "teamcontroller.h"
#include "config.h"
#include "team.h"
#include "conversationsmodel.h"
#include "usermanager.h"
#include "core_debug.h"
#include "future.h"
#include "networkdispatcher.h"
#include "messagemodel.h"

#include "utils/compat.h"
#include "utils/compiler.h"
#include "slackapi/conversations.h"
#include "slackapi/rtm.h"

#include <QMetaEnum>
#include <QMetaObject>
#include <QJsonDocument>

using namespace Shirk::Core;

QDebug operator<<(QDebug dbg, TeamController::Status status)
{
    const auto idx = TeamController::staticMetaObject.indexOfEnumerator("Status");
    Q_ASSERT(idx > -1);
    const auto enumerator = TeamController::staticMetaObject.enumerator(idx);
    return dbg.noquote() << enumerator.valueToKey(static_cast<int>(status));
}


TeamController::TeamController(std::unique_ptr<Team> team)
    : mTeam{std::move(team)}
    , mRTMController(mNetworkDispatcher)
    , mUserManager(*mTeam.get(), mNetworkDispatcher)
    , mConversations(*this)
    , mEmojiModel(mNetworkDispatcher)
{
    mNetworkDispatcher.setTeam(*mTeam.get());
}

TeamController::~TeamController() = default;

const Team &TeamController::team() const
{
    return *mTeam.get();
}

Team *TeamController::teamPtr()
{
    return mTeam.get();
}

Team &TeamController::team()
{
    return *mTeam.get();
}

TeamController::Status TeamController::status() const
{
    return mStatus;
}

void TeamController::setStatus(Status status)
{
    if (mStatus != status) {
        qCDebug(LOG_CORE) << "State changed to" << status;
        mStatus = status;
        Q_EMIT statusChanged(status);
    }
}

void TeamController::start()
{
    setStatus(Status::Connecting);

    mStartWatcher = FutureWatcher([this]() {
compiler_suppress_warning("-Wmissing-field-initializers")
        SlackAPI::PresenceSubscriptionRequest req{
            .ids = mConversations.getPresenceIds()
        };
compiler_restore_warning("-Wmissing-field-initializers")
        mRTMController.sendMessage(req.serialize());
    });

    mRTMController.start();
    Promise<void> rtmPromise;
    mStartWatcher(rtmPromise.getFuture());
    connect(&mRTMController, &RTMController::stateChanged,
            this, [this, rtmPromise = std::move(rtmPromise)](RTMController::State state) mutable {
                switch (state) {
                case RTMController::State::Connecting:
                    qCDebug(LOG_CORE) << "RTMController connecting ...";
                    break;
                case RTMController::State::Connected:
                    qCDebug(LOG_CORE) << "RTMController connected.";
                    rtmPromise.setResult();
                    break;
                case RTMController::State::Disconnected:
                    qCDebug(LOG_CORE) << "RTMController disconnected!";
                    mRTMController.stop();
                    break;
                }
            });
    mStartWatcher(mConversations.populate());
    mStartWatcher(mEmojiModel.populate());
}

void TeamController::quit()
{
    // TODO
    setStatus(Status::Disconnected);
}

void TeamController::updateConfig(Config &config)
{
    auto settings = config.settingsForTeam(mTeam->id());
    mTeam->updateConfig(settings.get());
}

MessageModel *TeamController::messageModelForConversation(Conversation *conv)
{
    return new MessageModel(*this, *conv);
}

void TeamController::joinChannel(Conversation *conv)
{
compiler_suppress_warning("-Wmissing-field-initializers")
    mNetworkDispatcher.sendRequest(SlackAPI::ConversationJoinRequest{ .channel = conv->id() })
        .then([conv](const QJsonValue &response) {
            const auto resp = SlackAPI::ConversationJoinResponse::parse(response);
            conv->updateFromConversation(resp.conversation);
        });
compiler_restore_warning("-Wmissing-field-initializers")
}

void TeamController::leaveChannel(Conversation *conv)
{
compiler_suppress_warning("-Wmissing-field-initializers")
    mNetworkDispatcher.sendRequest(SlackAPI::ConversationLeaveRequest{ .channel = conv->id() });
compiler_restore_warning("-Wmissing-field-initializers")
}
