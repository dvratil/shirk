// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "authcontroller.h"
#include "networkdispatcher.h"
#include "shirk_config.h"
#include "core_debug.h"
#include "slackapi/oauth.h"
#include "slackapi/teaminfo.h"
#include "utils/compat.h"
#include "utils/stringliterals.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <QDesktopServices>
#include <QUrl>
#include <QUrlQuery>
#include <QUuid>
#include <memory>

using namespace Shirk::Core;
using namespace Shirk::StringLiterals;

AuthController::AuthController()
{}

AuthController::~AuthController() = default;

void AuthController::setNetworkDispatcherOverride(NetworkDispatcher &dispatcher)
{
    mNetworkDispatcherOverride = dispatcher;
}

NetworkDispatcher &AuthController::networkDispatcher()
{
    return mNetworkDispatcherOverride.has_value()
            ? mNetworkDispatcherOverride->get()
            : mNetworkDispatcher;
}

Future<std::unique_ptr<Team>> AuthController::start()
{
    startServer();
    launchBrowser();

    return waitForCode()
        .then([this](const QString &code) {
            return exchangeCodeForToken(code);
        })
        .then([this](const SlackAPI::OAuthAccessResponse &resp) {
            shutdownServer();

            mTeam = std::make_unique<Team>(resp.team_id, resp.team_name, resp.access_token, resp.bot_access_token);
            networkDispatcher().setTeam(*mTeam.get());
            return fetchTeamInfo();
        })
        .then([this](const SlackAPI::TeamInfoResponse &response) {
            mTeam->updateFromTeamInfo(response);
            setState(State::Done);
            return std::move(mTeam);
        });
}

void AuthController::setState(State state)
{
    if (mState != state) {
        mState = state;
        Q_EMIT stateChanged(state);
    }
}

AuthController::State AuthController::state() const
{
    return mState;
}

void AuthController::setError(const QString &error)
{
    mError = error;
    setState(State::Error);
}

void AuthController::startServer()
{
    static constexpr const uint16_t port = 44916;

    mServer = std::make_unique<QTcpServer>();
    if (!mServer->listen(QHostAddress::Any, port)) {
        setError(tr("Failed to setup authentication flow."));
        return;
    }
    connect(mServer.get(), &QTcpServer::acceptError,
            this, [this](QAbstractSocket::SocketError) {
                setError(tr("Error during authentication: %1").arg(mServer->errorString()));
            });
}

void AuthController::launchBrowser()
{
    Q_ASSERT(mServer);

    mAuthState = QUuid::createUuid().toString(QUuid::WithoutBraces);
    QUrl url(u"https://slack.com/oauth/authorize"_qs);
    QUrlQuery query(url);
    query.addQueryItem(u"client_id"_qs, clientId);
    query.addQueryItem(u"scope"_qs, scopes.join(QLatin1Char(' ')));
    query.addQueryItem(u"redirect_uri"_qs, u"http://127.0.0.1:%1/"_qs.arg(mServer->serverPort()));
    query.addQueryItem(u"state"_qs, mAuthState);
    url.setQuery(query);

    if (!QDesktopServices::openUrl(url)) {
        setError(tr("Failed to launch browser."));
        return;
    }

    setState(State::WaitingForBrowser);
}

Future<QString> AuthController::waitForCode()
{
    Promise<QString> promise;
    auto future = promise.getFuture();

    Q_ASSERT(mServer);
    connect(mServer.get(), &QTcpServer::newConnection,
            this, [this, promise = std::move(promise)]() mutable {
                UniqueQObjectPtr<QTcpSocket> socket{mServer->nextPendingConnection()};
                readFromSocket(std::move(socket)).then([promise = std::move(promise)](const QString &code) mutable {
                    if (promise.isFulfilled()) {
                        return;
                    }
                    promise.setResult(code);
                });
            });

    return future;
}

namespace
{
struct Result {
    QString code;
    QString state;
};
std::optional<Result> parseCode(const QByteArray &data)
{
    const auto line = data.split(' ');
    if (line.size() != 3 || line.at(0) != QByteArray("GET") || !line.at(2).startsWith(QByteArray("HTTP/1.1"))) {
        return std::nullopt;
    }

    const QUrl url(QString::fromLatin1(line.at(1)));
    const QUrlQuery query(url);
    if (query.hasQueryItem(u"error"_qs)) {
        return std::nullopt;
    }
    return Result{query.queryItemValue(u"code"_qs), query.queryItemValue(u"state"_qs)};
}
}

Future<QString> AuthController::readFromSocket(UniqueQObjectPtr<QTcpSocket> socket)
{
    Promise<QString> promise;
    auto future = promise.getFuture();
    const auto s = socket.get();
    connect(s, &QTcpSocket::readyRead,
            this, [this, socket = std::move(socket), promise = std::move(promise)]() mutable {
                disconnect(socket.get());
                const auto data = socket->readLine();
                socket->readAll();
                socket->write("HTTP/1.1 200 OK\r\n");
                socket->flush();
                socket.reset();

                const auto result = parseCode(data);
                if (!result.has_value()) {
                    promise.setError(tr("Failed to receive authentication code from Slack."));
                } else if (result->state != mAuthState) {
                    promise.setError(tr("Security code mismatch."));
                } else {
                    promise.setResult(result->code);
                }
            });
    return future;
}

Future<Shirk::SlackAPI::OAuthAccessResponse> AuthController::exchangeCodeForToken(const QString &code)
{
    setState(State::RetrievingToken);
    return networkDispatcher().sendRequest(
                SlackAPI::OAuthAccessRequest{{}, clientId, clientSecret, code,
                                             u"http://127.0.0.1:%1/"_qs.arg(mServer->serverPort())}
           ).then([](const auto &data) {
                return SlackAPI::OAuthAccessResponse::parse(data);
           });
}

Future<Shirk::SlackAPI::TeamInfoResponse> AuthController::fetchTeamInfo()
{
    setState(State::RetrievingTeamInfo);
    return networkDispatcher().sendRequest(SlackAPI::TeamInfoRequest{{}, mTeam->id()})
            .then([](const auto &data) mutable {
                return SlackAPI::TeamInfoResponse::parse(data);
            });
}

void AuthController::shutdownServer()
{
    Q_ASSERT(mServer);
    mServer->close();
    mServer.reset();
}

QDebug operator<<(QDebug debug, AuthController::State state)
{
    auto dbg = debug.noquote();
    switch (state) {
    case AuthController::State::None:
        return dbg << "AuthController::State::None";
    case AuthController::State::Done:
        return dbg << "AuthController::State::Done";
    case AuthController::State::Error:
        return dbg << "AuthController::State::Error";
    case AuthController::State::RetrievingTeamInfo:
        return dbg << "AuthController::State::RetrievingTeamInfo";
    case AuthController::State::RetrievingToken:
        return dbg << "AuthController::State::RetrievingToken";
    case AuthController::State::WaitingForBrowser:
        return dbg << "AuthController::State::WaitingForBrowser";
    }

    Q_UNREACHABLE();
}
