// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "utils/compat.h"
#include "future.h"

#include <QAbstractListModel>
#include <QUrl>
#include <QMap>

#include <unordered_map>
#include <vector>
#include <variant>

namespace Shirk::Core
{
class NetworkDispatcher;

class EmojiModel : public QAbstractItemModel
{
    Q_OBJECT

    Q_PROPERTY(QStringList categories READ categories CONSTANT)
public:
    enum Roles {
        NameRole = Qt::UserRole + 1,
        UrlRole,
        CategoryRole,
        AliasesRole,
        IsCustomCategoryRole
    };

    explicit EmojiModel(NetworkDispatcher &dispatcher);

    Future<void> populate();

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QUrl urlForEmoji(QStringView emoji) const;

    QStringList categories() const;
private:
    bool isCustomCategory(const QModelIndex &index) const;

    struct Emoji {
        QString name;
        QStringList aliases;
        QUrl url;
    };
    struct Category {
        QString name;
        std::vector<Emoji> emojis;
    };

    QMap<QString /* alias */, std::pair<QString /* category */, QString /* emoji */>> mAliases;
    std::vector<Category> mCategories;

    NetworkDispatcher &mDispatcher;
};


} // namespace
