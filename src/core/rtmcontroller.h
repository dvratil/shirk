// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>

#include <memory>

#include "rtmsocket.h"
#include "slackapi/events.h"

namespace Shirk::Core
{

class NetworkDispatcher;
class RTMEventListener;

class RTMController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(RTMController::State state READ state NOTIFY stateChanged)
public:
    RTMController(NetworkDispatcher &networkDispatcher);
    ~RTMController() override;

    void start();
    void stop();

    enum class State {
        Disconnected,
        Connecting,
        Connected
    };
    State state() const {
        return mState;
    }

    void sendMessage(QJsonDocument msg);

    void subscribeListener(RTMEventListener *listener, SlackAPI::RTM::EventType event);
    void unsubscribeListener(RTMEventListener *listener, SlackAPI::RTM::EventType event);
    void unsubscribeListener(RTMEventListener *listener);
Q_SIGNALS:
    void stateChanged(State state);
    void error(const QString &error);
    void connectionLost();

private:
    void setState(State newState);
    void eventsAvailable();

    NetworkDispatcher &mNetworkDispatcher;
    State mState = State::Disconnected;

    RTMSocketPtr mSocket;

    std::array<std::vector<RTMEventListener *>, static_cast<std::size_t>(SlackAPI::RTM::EventType::_EventCount)> mSubscribers;
};


} // namespace
