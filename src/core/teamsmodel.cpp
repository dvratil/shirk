// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "teamsmodel.h"
#include "teamcontroller.h"
#include "team.h"
#include "config.h"
#include "core_debug.h"

Q_DECLARE_METATYPE(Shirk::Core::Team*)
Q_DECLARE_METATYPE(Shirk::Core::TeamController*)

using namespace Shirk::Core;

TeamsModel::~TeamsModel() = default;

void TeamsModel::loadControllers(Config &config)
{
    beginResetModel();
    const auto teamIds = config.teamIds();
    for (const auto &teamId : teamIds) {
        qCDebug(LOG_CORE) << "Loading team" << teamId;
        auto teamConfig = config.settingsForTeam(teamId);
        mControllers.push_back(std::make_unique<TeamController>(Team::fromSettings(teamConfig.get())));
        mControllers.back()->start();
        Q_EMIT teamAdded(mControllers.back().get());
    }
    endResetModel();
}

QHash<int, QByteArray> TeamsModel::roleNames() const
{
    auto roles = QAbstractListModel::roleNames();
    roles[TeamRole] = "team";
    roles[ControllerRole] = "controller";
    return roles;
}

int TeamsModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : mControllers.size();
}

QVariant TeamsModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= static_cast<int>(mControllers.size()) || index.column() != 0) {
        return {};
    }

    const auto &controller = mControllers[index.row()];
    switch (role) {
    case Qt::DisplayRole:
        return controller->team().name();
    case Qt::DecorationRole:
        return controller->team().icon();
    case TeamRole:
        return QVariant::fromValue(&controller->team());
    case ControllerRole:
        return QVariant::fromValue(controller.get());
    default:
        return {};
    }
}

TeamController &TeamsModel::controllerForIndex(const QModelIndex &index) const
{
    Q_ASSERT(index.row() >= 0 && index.row() < static_cast<int>(mControllers.size()));

    return *mControllers[index.row()].get();
}

struct FindController
{
    QStringView teamId;

    bool operator()(const std::unique_ptr<TeamController> &controller) const
    {
        return teamId == controller->team().id();
    }
};

TeamController &TeamsModel::controllerForTeam(const Team &team) const
{

    const auto ctrl = std::find_if(mControllers.cbegin(), mControllers.cend(), FindController{team.id()});
    Q_ASSERT(ctrl != mControllers.cend());

    return *ctrl->get();
}

TeamController &TeamsModel::controllerForTeam(QStringView team) const
{
    const auto ctrl = std::find_if(mControllers.cbegin(), mControllers.cend(), FindController{team});
    Q_ASSERT(ctrl != mControllers.cend());

    return *ctrl->get();
}

void TeamsModel::addTeamController(std::unique_ptr<TeamController> controller, Config &config)
{
    beginInsertRows({}, mControllers.size(), mControllers.size());
    controller->updateConfig(config);
    controller->start();
    mControllers.push_back(std::move(controller));
    endInsertRows();

    Q_EMIT teamAdded(mControllers.back().get());
}
