// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "emojimodel.h"
#include "networkdispatcher.h"
#include "slackapi/emoji.h"
#include "core_debug.h"
#include "utils/stringliterals.h"
#include "utils/compiler.h"

#include "compiled_emoji.h"

#include <iterator>
#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/range_fwd.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/filter.hpp>
#include "utils/ranges.h"

#include <KLocalizedString>

using namespace Shirk::Core;
using namespace Shirk::StringLiterals;

namespace {
bool isRoot(const QModelIndex &index) { return !index.isValid(); }
bool isCategory(const QModelIndex &index) { return index.isValid() && index.internalId() == 0; }
bool isEmoji(const QModelIndex &index) { return index.isValid() && index.internalId() > 0; }

constexpr static const auto customCategoryName = I18N_NOOP("Custom");

}

EmojiModel::EmojiModel(NetworkDispatcher &dispatcher)
    : mDispatcher(dispatcher)
{}

Future<void> EmojiModel::populate()
{
compiler_suppress_warning("-Wmissing-field-initializers")
    SlackAPI::EmojiListRequest request{.include_categories = true};
compiler_restore_warning("-Wmissing-field-initializers")
    return mDispatcher.sendRequest(std::move(request))
        .then([this](const QJsonValue &data) {
            const auto resp = SlackAPI::EmojiListResponse::parse(data);
            beginResetModel();
            mCategories.clear();
            mAliases.clear();
            auto &customCategory = mCategories.emplace_back(Category{i18n(customCategoryName), {}});
            for (auto emoji = resp.emojis.cbegin(), end = resp.emojis.cend(); emoji != end; ++emoji) {
                if (emoji.value().startsWith(u"alias:"_qsv)) {
                    mAliases.insert(emoji.key(), {customCategory.name, emoji.value().mid(u"alias:"_qsv.size())});
                } else {
                    customCategory.emojis.emplace_back(Emoji{emoji.key(), {}, emoji.value()});
                }
            }

            for (auto categoryIt = resp.categories.cbegin(), end = resp.categories.cend(); categoryIt != end; ++categoryIt) {
                auto &category = mCategories.emplace_back(Category{categoryIt.key(), {}});
                for (const auto &name: categoryIt.value()) {
                    const auto emoji = ranges::find_if(Shirk::Emoji::s_emojis, [&name](const Shirk::Emoji::Emoji &emoji) {
                        return emoji.name() == name;
                    });
                    if (emoji == Shirk::Emoji::s_emojis.end()) {
                        qCWarning(LOG_CORE) << "Couldn't find emoji" << name << "in the internal emoji table!";
                        continue;
                    }

                    const auto emojiAliases = emoji->aliases();
                    const auto a = emojiAliases | ranges::views::filter([](QStringView f) { return !f.isEmpty(); })
                                                | ranges::views::transform([](QStringView f) { return f.toString(); })
                                                | ranges::to_qt<QStringList>();
                    category.emojis.emplace_back(Emoji{name, a, QUrl(emoji->image().toString())});
                    for (const auto &alias : a) {
                        mAliases.insert(alias, {category.name, name});
                    }
                }
            }
            endResetModel();
        });
}

QHash<int, QByteArray> EmojiModel::roleNames() const
{
    return {
        {CategoryRole, "category"},
        {NameRole, "name"},
        {UrlRole, "url"},
        {AliasesRole, "aliases"},
        {IsCustomCategoryRole, "isCustomCategory"}
    };
}

int EmojiModel::rowCount(const QModelIndex &parent) const
{
    if (isRoot(parent)) {
        return mCategories.size();
    } else if (isCategory(parent)) {
        return mCategories[parent.row()].emojis.size();
    }

    return 0;
}

int EmojiModel::columnCount(const QModelIndex &parent) const
{
    if (isRoot(parent)) {
        return 1;
    } else if (isCategory(parent)) {
        return 1;
    }

    return 0;
}

QModelIndex EmojiModel::index(int row, int column, const QModelIndex &parent) const
{
    if (isRoot(parent)) {
        return createIndex(row, column);
    } else if (isCategory(parent)) {
        return createIndex(row, column, parent.row() + 1);
    }

    return {};
}

QModelIndex EmojiModel::parent(const QModelIndex &child) const
{
    if (isEmoji(child)) {
        return createIndex(child.internalId() - 1, 0);
    }

    return {};
}

QVariant EmojiModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(index); Q_UNUSED(role);

    if (isCategory(index)) {
        switch (role) {
        case Qt::DisplayRole:
        case CategoryRole:
            return mCategories[index.row()].name;
        }
    } else if (isEmoji(index)) {
        const auto &category = mCategories[index.internalId() - 1];
        const auto &emoji = category.emojis[index.row()];
        switch (role) {
        case Qt::DisplayRole:
        case NameRole:
            return emoji.name;
        case UrlRole:
            return emoji.url;
        case CategoryRole:
            return category.name;
        case AliasesRole:
            return emoji.aliases;
        case IsCustomCategoryRole:
            return category.name == i18n(customCategoryName);
        }
    }

    return {};
}

QStringList EmojiModel::categories() const
{
    return mCategories | ranges::views::transform([](const Category &cat) { return cat.name; })
                       | ranges::to_qt<QStringList>();
}

QUrl EmojiModel::urlForEmoji(QStringView emojiName) const
{
    const auto &customEmoji = mCategories.front();
    const auto emojiIt = ranges::find_if(customEmoji.emojis, [&emojiName](const auto &emoji) {
        return emoji.name == emojiName;
    });
    if (emojiIt == customEmoji.emojis.end()) {
        return {};
    }

    return emojiIt->url;
}
