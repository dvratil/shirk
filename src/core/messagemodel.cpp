// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "messagemodel.h"
#include "conversations.h"
#include "events.h"
#include "teamcontroller.h"
#include "conversation.h"
#include "networkdispatcher.h"
#include "user.h"
#include "utils/compiler.h"
#include "slackapi/message.h"
#include "core_debug.h"

#include <algorithm>
#include <functional>

#include <iterator>
#include <range/v3/algorithm/lower_bound.hpp>
#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/range/conversion.hpp>

using namespace Shirk;
using namespace Shirk::Core;
using namespace ranges;

Q_DECLARE_METATYPE(QQmlListProperty<Shirk::Core::User>)
Q_DECLARE_METATYPE(QQmlListProperty<Shirk::Core::Reaction>)

template<typename T, typename Obj, typename MemberVar>
QQmlListProperty<T> listProperty(Obj *object, MemberVar data)
{
    using MemberType = decltype(&(object->*data));
    return QQmlListProperty<T>(object, &(object->*data),
                [](QQmlListProperty<T> *list) -> int {
                    return static_cast<MemberType>(list->data)->size();
                },
                [](QQmlListProperty<T> *list, int index) {
                    return (*static_cast<MemberType>(list->data))[index].get();
                });
}


Reaction::Reaction(const SlackAPI::Reaction &in, UserManager &userManager)
    : mName{in.name}
    , mUsers(in.users | view::transform(User::factory(userManager)) | to<std::vector>())
    , mCount{in.count}
{}

Reaction::Reaction(const SlackAPI::RTM::ReactionAddedEvent &in, UserManager &userManager)
    : mName{in.reaction}
    , mUsers{{std::make_unique<User>(in.user, userManager)}}
    , mCount{1}
{}

void Reaction::update(const SlackAPI::RTM::ReactionAddedEvent &event, UserManager &userManager)
{
    Q_ASSERT(mName == event.reaction);
    mCount++;
    mUsers.emplace_back(std::make_unique<User>(event.user, userManager));
}

QQmlListProperty<User> Reaction::users()
{
    return listProperty<User>(this, &Reaction::mUsers);
}

Message::Message(const SlackAPI::Message &in, UserManager &userManager)
    : mTs{in.ts}
    , mText{in.text}
    , mUser{std::make_unique<User>(in.user, userManager)}
    , mReplyCount{in.reply_count}
    , mLatestReply{in.latest_reply}
    , mReplyUsers(in.reply_users | view::transform(User::factory(userManager)) | to<std::vector>())
    , mReactions(in.reactions | view::transform([&userManager](const auto &reaction) { return std::make_unique<Reaction>(reaction, userManager); }) | to<std::vector>())
{}

Message::Message(const SlackAPI::RTM::MessageEvent &in, UserManager &userManager)
    , mTs{in.ts}
    , mText{in.text}
    , mUser{std::make_unique<User>(in.user, userManager)}
{}

QQmlListProperty<User> Message::replyUsers()
{
    return listProperty<User>(this, &Message::mReplyUsers);
}

QQmlListProperty<Reaction> Message::reactions()
{
    return listProperty<Reaction>(this, &Message::mReactions);
}

void Message::update(const SlackAPI::RTM::ReactionAddedEvent &e, UserManager &userManager)
{
    auto reaction = ranges::find_if(mReactions, [&e](const auto &reaction) { return reaction->name() == e.reaction; });
    if (reaction == mReactions.end()) {
        mReactions.emplace_back(Reaction::parse(e, userManager));
    } else {
        (*reaction)->update(e, userManager);
    }
}

MessageModel::MessageModel(TeamController &controller, Conversation &conversation)
    : QAbstractListModel()
    , RTMEventListener(controller.rtmController())
    , mController(controller), mConversation(conversation)
{
    qRegisterMetaType<QQmlListProperty<User>>();
    qRegisterMetaType<QQmlListProperty<Reaction>>();
    subscribeTo(SlackAPI::RTM::EventType::Message);
    subscribeTo(SlackAPI::RTM::EventType::ReactionAdded);

    fetchOlder();
}

void MessageModel::fetchOlder()
{
compiler_suppress_warning("-Wmissing-field-initializers")
    SlackAPI::ConversationHistoryRequest req {
        .channel = mConversation.id(),
        .inclusive = mOldest.has_value()
        .latest = mOldest
    };
compiler_restore_warning("-Wmissing-field-initializers")
    mController.networkDispatcher().sendRequest(std::move(req)).then([this](const QJsonValue &resp) {
        handleResponse(resp);
    });
}

QHash<int, QByteArray> MessageModel::roleNames() const
{
    auto roles = QAbstractListModel::roleNames();
    roles[static_cast<int>(Role::Message)] = "message";
    roles[static_cast<int>(Role::Date)] = "date";
    return roles;
}

int MessageModel::rowCount(const QModelIndex &) const
{
    return mMessages.size();
}

QVariant MessageModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= static_cast<int>(mMessages.size())) {
        return {};
    }

    auto &message = mMessages[index.row()];
    if (static_cast<Role>(role) == Role::Message) {
        return QVariant::fromValue(message.get());
    } else if (static_cast<Role>(role) == Role::Date) {
        return message->ts();
    } else {
        return {};
    }
}

void MessageModel::handleResponse(const QJsonValue &resp)
{
    using namespace std::placeholders;

    const auto response = SlackAPI::ConversationHistoryResponse::parse(resp);
    if (response.messages.empty()) {
        return;
    }

    std::vector<std::unique_ptr<Message>> newMessages;
    newMessages.reserve(response.messages.size() + mMessages.size());
    std::transform(response.messages.cbegin(), response.messages.cend(),
                   std::back_inserter(newMessages),
                   [this](const SlackAPI::Message &msg) {
                        return Message::parse(msg, mController.userManager());
                   });
    std::sort(newMessages.begin(), newMessages.end(), [](const auto &lm, const auto &rm) {
            return lm->ts() < rm->ts();
    });

    if (mMessages.empty()) {
        beginResetModel();
        std::swap(newMessages, mMessages);
        mOldest = mMessages.front()->ts();
        endResetModel();
    } else if (response.latest < mOldest) {
        beginInsertRows({}, 0, response.messages.size() - 1);
        std::move(mMessages.begin(), mMessages.end(), std::back_inserter(newMessages));
        std::swap(newMessages, mMessages);
        mOldest = mMessages.front()->ts();
        endInsertRows();
    } else if (newMessages.front()->ts() > mMessages.back()->ts()) {
        beginInsertRows({}, mMessages.size(), mMessages.size() + newMessages.size());
        std::move(newMessages.begin(), newMessages.end(), std::back_inserter(mMessages));
        endInsertRows();
    } else {
        Q_ASSERT(false);
        // TODO
    }
}

void MessageModel::rtmMessageEvent(const SlackAPI::RTM::MessageEvent &event)
{
    if (event.channel != mConversation.id()) {
        return;
    }

    if (mMessages.empty() || event.ts > mMessages.back()->ts()) {
        beginInsertRows({},mMessages.size(), mMessages.size());
        mMessages.emplace_back(Message::parse(event, mController.userManager()));
        endInsertRows();
    } else {
        auto newMessage = Message::parse(event, mController.userManager());
        const auto it = ranges::lower_bound(mMessages, newMessage, [](const auto &m, const auto &e) { return m->ts() < e->ts(); });
        const auto pos = std::distance(mMessages.begin(), it);
        beginInsertRows({}, pos, pos);
        mMessages.emplace(it, std::move(newMessage));
        endInsertRows();
    }

    mOldest = mMessages.front()->ts();
}

void MessageModel::rtmReactionAddedEvent(const SlackAPI::RTM::ReactionAddedEvent &event)
{
    if (event.item.channel != mConversation.id()) {
        return;
    }

    auto msg = ranges::find_if(mMessages, [&event](const auto &msg) { return msg->ts() == event.item.ts; });
    if (msg == mMessages.end()) {
        qCWarning(LOG_CORE) << "Couldn't find message" << event.item.ts << "in channel" << event.item.channel;
        qCWarning(LOG_CORE) << "Candidates are: ";
        for (const auto &msg : mMessages) {
            qCWarning(LOG_CORE) << "\t" << msg->ts();
        }
        return;
    }

    (*msg)->update(event, mController.userManager());
    const auto idx = index(std::distance(mMessages.begin(), msg), 0);
    Q_EMIT dataChanged(idx, idx);
}

