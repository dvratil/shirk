// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QPointer>
#include <QJsonValue>
#include <QUrlQuery>
#include <QJsonObject>

#include <memory>
#include <vector>
#include <deque>
#include <functional>

#include "team.h"
#include "future.h"
#include "slackapi/common.h"
#include "utils/memory.h"

namespace Shirk::Core
{

class Config;

class NetworkDispatcher : public QObject
{
    Q_OBJECT

public:
    NetworkDispatcher();
    ~NetworkDispatcher() override;

    void setTeam(const Team &team);

    template<typename Msg>
    Future<QJsonValue> sendRequest(Msg &&msg);

protected:
    struct Request {
        SlackAPI::Method method;
        QUrl url;
        Promise<QJsonValue> promise;
    };
    virtual void dispatchRequest(Request &&request);

private:
    Future<QJsonValue> enqueueRequest(SlackAPI::Method method, const QUrl &url);
    QUrl urlForEndpoint(QStringView endpoint, const QUrlQuery &query, std::optional<QString> token = std::nullopt) const;
    void tryDispatchNextRequest();

    template<typename Msg>
    std::optional<QString> getTokenForMsg() const;

    std::size_t mMaxRunningRequests = 5;
    QNetworkAccessManager mNam;

    std::deque<Request> mPendingRequests;
    std::vector<UniqueQObjectPtr<QNetworkReply>> mRunningRequests;

    std::optional<std::reference_wrapper<const Team>> mTeam;
};

template<typename Msg>
Future<QJsonValue> NetworkDispatcher::sendRequest(Msg &&msg)
{
    using MsgT = std::decay_t<Msg>;
    return enqueueRequest(MsgT::method, urlForEndpoint(MsgT::endpoint, msg.serialize(), getTokenForMsg<Msg>()));
}

template<typename Msg>
std::optional<QString> NetworkDispatcher::getTokenForMsg() const
{
    if constexpr (std::is_base_of_v<SlackAPI::UserAuthenticatedRequest, std::decay_t<Msg>>) {
        Q_ASSERT(mTeam.has_value());
        return mTeam->get().accessToken();
    } else if constexpr (std::is_base_of_v<SlackAPI::BotAuthenticatedRequest, std::decay_t<Msg>>) {
        Q_ASSERT(mTeam.has_value());
        return mTeam->get().botAccessToken();
    } else {
        return std::nullopt;
    }
}

} // namespace
