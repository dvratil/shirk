// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "rtmeventlistener.h"
#include "rtmcontroller.h"
#include "core_debug.h"
#include "slackapi/events.h"

#include <range/v3/algorithm/for_each.hpp>

using namespace Shirk::Core;
using namespace ranges;

RTMEventListener::RTMEventListener(RTMController &controller)
    : mController(controller)
{}

RTMEventListener::~RTMEventListener()
{
    mController.unsubscribeListener(this);
}

void RTMEventListener::rtmEvent(const SlackAPI::RTM::Event &event)
{
    using Type = SlackAPI::RTM::EventType;

    #define CASE(type) \
    case Type::type: \
        rtm ## type ## Event(static_cast<const SlackAPI::RTM::type ## Event &>(event)); \
        break;

    switch (event.eventType()) {
    CASE(Message)
    CASE(MemberJoinedChannel)
    CASE(ChannelLeft)
    CASE(ReactionAdded)
    default:
        qCWarning(LOG_CORE) << "RTM event " << event.eventType() << " is not supported";
        Q_ASSERT(false);
        break;
    }

    #undef CASE
}

void RTMEventListener::subscribeTo(std::vector<SlackAPI::RTM::EventType> types)
{
    for_each(types, [this](auto event) { subscribeTo(event); });
}

void RTMEventListener::subscribeTo(SlackAPI::RTM::EventType type)
{
    mController.subscribeListener(this, type);
}

#define DEFAULT_EVENT_HANDLER(event) \
    void RTMEventListener::rtm##event(const SlackAPI::RTM::event &) \
    { \
        qCCritical(LOG_CORE, "Default implementation of " #event " event handler called!"); \
    }

DEFAULT_EVENT_HANDLER(MessageEvent)
DEFAULT_EVENT_HANDLER(MemberJoinedChannelEvent)
DEFAULT_EVENT_HANDLER(ChannelLeftEvent)
DEFAULT_EVENT_HANDLER(ReactionAddedEvent)
