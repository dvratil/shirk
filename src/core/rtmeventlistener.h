// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "events.h"
#include <tuple>
#include <type_traits>
#include <vector>

namespace Shirk::SlackAPI::RTM
{
enum class EventType;
struct Event;
struct MessageEvent;
struct MemberJoinedChannelEvent;
struct ChannelLeftEvent;
struct ReactionAddedEvent;
}

namespace Shirk::Core
{

class RTMController;

class RTMEventListener
{
    friend class RTMController;
public:
    explicit RTMEventListener(RTMController &controller);
    virtual ~RTMEventListener();

    void subscribeTo(std::vector<SlackAPI::RTM::EventType> types);
    void subscribeTo(SlackAPI::RTM::EventType type);

protected:
    virtual void rtmEvent(const SlackAPI::RTM::Event &event);

    virtual void rtmMessageEvent(const SlackAPI::RTM::MessageEvent &event);
    virtual void rtmMemberJoinedChannelEvent(const SlackAPI::RTM::MemberJoinedChannelEvent &event);
    virtual void rtmChannelLeftEvent(const SlackAPI::RTM::ChannelLeftEvent &event);
    virtual void rtmReactionAddedEvent(const SlackAPI::RTM::ReactionAddedEvent &event);

private:
    RTMController &mController;
};


} // namespace
