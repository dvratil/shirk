// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>
#include <QDateTime>
#include <QFlags>

#include "slackapi/common.h"
#include "user.h"

namespace Shirk::SlackAPI {
struct Conversation;
}

namespace Shirk::Core
{

struct ConversationInfo {
    QString value;
    std::unique_ptr<User> creator;
    QDateTime lastSet;
};

} // namespace

namespace Shirk::Core
{
class UserManager;
class Conversation : public QObject
{
    Q_OBJECT

public:
    enum class Type {
        Channel, /// public channel
        IM,      /// private conversation between two users or user and bot
        MPIM,    /// private unnamed conversation between multiple users
        Group    /// private channel
    };
    Q_ENUM(Type)


    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString name READ name NOTIFY conversationChanged)
    Q_PROPERTY(Shirk::Core::User* user READ user NOTIFY conversationChanged)
    Q_PROPERTY(QStringList previousNames READ previousNames NOTIFY conversationChanged)
    Q_PROPERTY(Shirk::Core::User* creator READ creator CONSTANT)
    Q_PROPERTY(int memberCount READ memberCount NOTIFY conversationChanged)

    Q_PROPERTY(QDateTime lastRead READ lastRead NOTIFY lastReadChanged)
    Q_PROPERTY(QDateTime created READ created CONSTANT)
    Q_PROPERTY(int unreadCount READ unreadCount NOTIFY unreadCountChanged)
    Q_PROPERTY(int unreadCountDisplay READ unreadCountDisplay NOTIFY unreadCountChanged)
    Q_PROPERTY(QDateTime latestMessage READ latestMessage NOTIFY latestMessageChanged)

    Q_PROPERTY(bool isMember READ isMember NOTIFY conversationChanged)
    Q_PROPERTY(bool isArchive READ isArchive NOTIFY conversationChanged)
    Q_PROPERTY(bool isReadOnly READ isReadOnly NOTIFY conversationChanged)

    Q_PROPERTY(bool isGeneral READ isGeneral NOTIFY conversationChanged)
    Q_PROPERTY(bool isChannel READ isChannel NOTIFY conversationChanged)
    Q_PROPERTY(bool isGroup READ isGroup NOTIFY conversationChanged)
    Q_PROPERTY(bool isIM READ isIM NOTIFY conversationChanged)
    Q_PROPERTY(bool isMPIM READ isMPIM NOTIFY conversationChanged)
    Q_PROPERTY(bool isPrivate READ isPrivate NOTIFY conversationChanged)
    Q_PROPERTY(Shirk::Core::Conversation::Type type READ type NOTIFY conversationChanged)

    Q_PROPERTY(bool isShared READ isShared NOTIFY conversationChanged)

    Q_PROPERTY(QString topic READ topic NOTIFY conversationChanged)
    Q_PROPERTY(Shirk::Core::User* topicCreator READ topicCreator NOTIFY conversationChanged)
    Q_PROPERTY(QDateTime topicLastSet READ topicLastSet NOTIFY conversationChanged)

    Q_PROPERTY(QString purpose READ purpose NOTIFY conversationChanged)
    Q_PROPERTY(Shirk::Core::User* purposeCreator READ purposeCreator NOTIFY conversationChanged)
    Q_PROPERTY(QDateTime purposeLastSet READ purposeLastSet NOTIFY conversationChanged)

public:
    Conversation(const QString &id, UserManager &userManager);
    ~Conversation() = default;

    QString id() const { return mId; }
    QString name() const { return mName; }
    User *user() { return mUser.get(); }
    QStringList previousNames() const { return mPreviousNames; }
    User *creator() { return mCreator.get(); }

    int memberCount() const { return mMemberCount; }
    int unreadCount() const { return mUnreadCount; }
    int unreadCountDisplay() const { return mUnreadCountDisplay; }

    QDateTime lastRead() const { return mLastRead; }
    QDateTime created() const { return mCreated; }
    QDateTime latestMessage() const { return mLatestMessage; }

    bool isMember() const;
    void setIsMember(bool member);

    bool isArchive() const { return mIsArchive; }
    bool isReadOnly() const { return mIsReadOnly; }
    bool isGeneral() const { return mIsGeneral; }
    bool isChannel() const { return mIsChannel; }
    bool isGroup() const { return mIsGroup; }
    bool isIM() const { return mIsIM; }
    bool isMPIM() const { return mIsMPIM; }
    bool isPrivate() const { return mIsPrivate; }
    bool isShared() const { return mIsShared; }

    Type type() const;

    QString topic() const { return mTopic.value; }
    User *topicCreator() { return mTopic.creator.get(); }
    QDateTime topicLastSet() const { return mTopic.lastSet; }

    QString purpose() const { return mPurpose.value; }
    User *purposeCreator() { return mPurpose.creator.get(); }
    QDateTime purposeLastSet() const { return mPurpose.lastSet; }

    void updateFromConversation(const SlackAPI::Conversation &conv);

Q_SIGNALS:
    void conversationChanged();
    void lastReadChanged();
    void unreadCountChanged();
    void latestMessageChanged();

private:
    QString mId;
    QString mName;
    std::unique_ptr<User> mUser;
    QStringList mPreviousNames;
    std::unique_ptr<User> mCreator;
    int mMemberCount = 0;
    int mUnreadCount = 0;
    int mUnreadCountDisplay = 0;
    QDateTime mLastRead;
    QDateTime mCreated;
    QDateTime mLatestMessage;
    ConversationInfo mTopic;
    ConversationInfo mPurpose;
    bool mIsMember = false;
    bool mIsArchive = false;
    bool mIsReadOnly = false;
    bool mIsGeneral = false;
    bool mIsChannel = false;
    bool mIsGroup = false;
    bool mIsIM = false;
    bool mIsMPIM = false;
    bool mIsPrivate = false;
    bool mIsShared = false;

    UserManager &mUserManager;
};

} // namespace

