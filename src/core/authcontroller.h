// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>
#include <QDebug>

#include <functional>
#include <optional>
#include <memory>

#include "networkdispatcher.h"
#include "future.h"
#include "utils/memory.h"
#include "team.h"

class QTcpServer;
class QTcpSocket;

namespace Shirk::SlackAPI {
class OAuthAccessResponse;
class TeamInfoResponse;
}

namespace Shirk::Core {

class AuthController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(State state READ state NOTIFY stateChanged)
public:
    using Token = QString;
    using Error = QString;

    enum class State {
        None,
        WaitingForBrowser,
        RetrievingToken,
        RetrievingTeamInfo,
        Done,
        Error
    };
    Q_ENUM(State)

    explicit AuthController();
    ~AuthController() override;

    Future<std::unique_ptr<Team>> start();

    State state() const;

    void setNetworkDispatcherOverride(NetworkDispatcher &networkDispatcher);

Q_SIGNALS:
    void stateChanged(State state);

private:
    Q_DISABLE_COPY_MOVE(AuthController)

    QString generateState();
    void setState(State state);
    void setError(const Error &error);

    void startServer();
    void shutdownServer();
    void launchBrowser();
    Future<QString> waitForCode();
    Future<SlackAPI::OAuthAccessResponse> exchangeCodeForToken(const QString &code);
    Future<QString> readFromSocket(UniqueQObjectPtr<QTcpSocket> socket);
    Future<SlackAPI::TeamInfoResponse> fetchTeamInfo();

    NetworkDispatcher &networkDispatcher();

    State mState = State::None;
    std::unique_ptr<Team> mTeam;
    Error mError;
    QString mAuthState;

    std::unique_ptr<QTcpServer> mServer;

    NetworkDispatcher mNetworkDispatcher;
    std::optional<std::reference_wrapper<NetworkDispatcher>> mNetworkDispatcherOverride;
};

}

QDebug operator<<(QDebug, Shirk::Core::AuthController::State);
