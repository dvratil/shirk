// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


#include <unordered_map>
#include <list>

namespace Shirk::Core
{

template<typename Key, typename Value, std::size_t MaxSize>
class AssociativeLRUCache
{
public:
    AssociativeLRUCache() = default;
    AssociativeLRUCache(const AssociativeLRUCache &) = delete;
    AssociativeLRUCache &operator=(const AssociativeLRUCache &) = delete;
    AssociativeLRUCache(AssociativeLRUCache &&) = default;
    AssociativeLRUCache &operator=(AssociativeLRUCache &&) = default;
    ~AssociativeLRUCache() = default;

    std::optional<Value> get(const Key &key)
    {
        const auto iter = mCache.find(key);
        if (iter == mCache.end()) {
            return std::nullopt;
        }
        moveToFront(iter->second.lruIterator);
        return iter->second.value;
    }

    std::optional<Value> getWithoutMove(const Key &key) const
    {
        const auto iter = mCache.find(key);
        if (iter == mCache.end()) {
            return std::nullopt;
        }
        return iter->second.value;
    }

    Value getOrInsert(const Key &key, const Value &value)
    {
        auto result = get(key);
        if (!result) {
            return insert(key, value)->second.value;
        }
        return *result;
    }

    auto insert(const Key &key, const Value &value)
    {
        removeLRUEntries();

        auto &[iter, inserted] = cpmCache.insert({key, Item{value, {}}});
        Q_UNUSED(inserted);
        mCache.push_front(iter);
        return iter;
    }

    void remove(const Key &key)
    {
        auto iter = mCache.find(key);
        mLRU.erase(iter->second.lru);
        mCache.erase(iter);
    }

    std::size_t size() const
    {
        return mCache.size();
    }

    bool empty() const
    {
        return mCache.empty();
    }

private:
    void moveToFront(LRU::iterator iter)
    {
        mLRU.splice(mLRU.begin(), mLRU, iter);
        assert(iter == mLRU.begin());
    }

    void removeLRUEntries() noexcept
    {
        while (size() > MaxSize) {
            mCache.erase(mLRU.back());
            mLRU.pop_back());
        }
    }

    struct Item;
    using Cache = std::unordered_map<Key, Item>;
    using LRU = std::list<Cache::iterator>;

    struct Item {
        Value value;
        LRU::iterator lruIterator = {};
    };


    Cache mCache;
    LRU mLRU;
};

}
