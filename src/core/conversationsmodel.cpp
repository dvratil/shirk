// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "conversationsmodel.h"
#include "rtmeventlistener.h"
#include "teamcontroller.h"
#include "networkdispatcher.h"
#include "slackapi/conversations.h"
#include "slackapi/events.h"
#include "utils/memory.h"
#include "utils/compiler.h"

#include <range/v3/view/filter.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/algorithm/find_if.hpp>

#include "utils/ranges.h"

#include <memory>

using namespace Shirk::Core;
using namespace ranges;

Q_DECLARE_METATYPE(Shirk::Core::Conversation*)

ConversationsModel::ConversationsModel(TeamController &controller)
    : QAbstractItemModel()
    , RTMEventListener(controller.rtmController())
    , mController(controller)
{
    subscribeTo({SlackAPI::RTM::ChannelLeftEvent::type,
                 SlackAPI::RTM::MemberJoinedChannelEvent::type});
}

Future<void> ConversationsModel::populate()
{
    Promise<void> promise;
    auto future = promise.getFuture();
// FIXME: I suspect this is just GCC being overly-sensitive
compiler_suppress_warning("-Wmissing-field-initializers")
    SlackAPI::ConversationsListRequest request{
        .types = {SlackAPI::ConversationsListRequest::Type::PublicChannel,
                  SlackAPI::ConversationsListRequest::Type::PrivateChannel,
                  SlackAPI::ConversationsListRequest::Type::MPIM,
                  SlackAPI::ConversationsListRequest::Type::IM}
    };
compiler_restore_warning("-Wmissing-field-initializers")
    mController.networkDispatcher().sendRequest(std::move(request))
        .then([this, promise = std::move(promise)](const QJsonValue &value) mutable {
            const auto resp = SlackAPI::ConversationsListResponse::parse(value);
            beginResetModel();
            mEntries.clear();
            for (const auto &chann: resp.channels) {
                auto conv = std::make_unique<Conversation>(chann.id, mController.userManager());
                conv->updateFromConversation(chann);
                mEntries.emplace_back(std::move(conv));
                connect(mEntries.back().get(), &Conversation::conversationChanged,
                        this, [this, entry = mEntries.back().get()]() {
                            const auto pos = std::distance(mEntries.cbegin(), std::find_if(mEntries.cbegin(), mEntries.cend(),
                                                           [entry](const auto &conv) { return conv.get() == entry; }));
                            Q_EMIT dataChanged(createIndex(pos, 0), createIndex(pos, 1));
                        });
            }
            endResetModel();
            promise.setResult();
        });

    return future;
}

ConversationsModel::Group ConversationsModel::groupForConversation(const Conversation *conv) const
{
    switch (conv->type()) {
    case Conversation::Type::Channel:
    case Conversation::Type::Group:
        return Group::Channels;
    case Conversation::Type::IM:
    case Conversation::Type::MPIM:
        return Group::DirectChats;
    }

    return Group::Channels;
}

QHash<int, QByteArray> ConversationsModel::roleNames() const
{
    auto roles = QAbstractItemModel::roleNames();
    roles[ConversationRole] = "conversation";
    roles[GroupRole] = "group";
    roles[IsMemberRole] = "isMember";
    return roles;
}

int ConversationsModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return mEntries.size();
    }

    return 0;
}

int ConversationsModel::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return 2;
    }

    return 0;
}

QModelIndex ConversationsModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        if (row < 0 || row >= static_cast<int>(mEntries.size())
                || column < 0 || column >= columnCount(parent)) {
            return {};
        }
        return createIndex(row, column, nullptr);
    }

    return {};
}

QModelIndex ConversationsModel::parent(const QModelIndex &) const
{
    return {};
}

QVariant ConversationsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() < 0 || index.row() >= static_cast<int>(mEntries.size())) {
        return {};
    }

    const auto &conversation = mEntries[index.row()];

    switch (role) {
    case Qt::DisplayRole:
        switch (index.column()) {
        case 0:
            switch (conversation->type()) {
            case Conversation::Type::Channel:
            case Conversation::Type::Group:
                return conversation->name();
            case Conversation::Type::IM:
            case Conversation::Type::MPIM:
                return conversation->user()->name();
            }
            return {};
        case 1:
            return conversation->unreadCountDisplay();
        }
        return {};
    case ConversationRole:
        return QVariant::fromValue(conversation.get());
    case GroupRole:
        return QVariant::fromValue(groupForConversation(conversation.get()));
    case IsMemberRole:
        return conversation->isMember();
    default:
        return {};
    }

    return {};
}

QStringList ConversationsModel::getPresenceIds() const
{
    return mEntries | views::filter([](const auto &conv) { return conv->type() == Conversation::Type::IM; })
                    | views::transform([](const auto &conv) -> QString { return conv->user()->id(); })
                    | to_qt<QStringList>();
}

void ConversationsModel::rtmChannelLeftEvent(const SlackAPI::RTM::ChannelLeftEvent &e)
{
    auto conv = find_if(mEntries, [&e](const auto &conv) { return conv->id() == e.channel; });
    (*conv)->setIsMember(false);
}

void ConversationsModel::rtmMemberJoinedChannelEvent(const SlackAPI::RTM::MemberJoinedChannelEvent &e)
{
    auto conv = find_if(mEntries, [&e](const auto &conv) { return conv->id() == e.channel; });
    (*conv)->setIsMember(true);
}
