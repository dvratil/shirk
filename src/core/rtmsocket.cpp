// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "rtmsocket.h"
#include "slackapi/events.h"
#include "utils/stringliterals.h"

#include <QThread>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSslConfiguration>
#include <qssl.h>

using namespace Shirk;
using namespace Shirk::Core;
using namespace Shirk::StringLiterals;
using namespace std::chrono_literals;

void RTMSocketDeleter::operator()(RTMSocket *socket)
{
    socket->deleteLater();
    socket->thread()->quit();
    socket->thread()->wait();
    socket->thread()->deleteLater();
}

RTMSocket::RTMSocket(const QUrl &url)
{
    auto thread = new QThread(this);
    moveToThread(thread);
    thread->start();

    QMetaObject::invokeMethod(this, [this, url]() { init(url); }, Qt::QueuedConnection);
}

void RTMSocket::init(const QUrl &url)
{
    Q_ASSERT(thread() == QThread::currentThread());

    mData = std::make_unique<Data>();
    auto conf = mData->socket.sslConfiguration();
    // Starting March, Slack will not support TLS 1.0 and 1.1
    conf.setProtocol(QSsl::TlsV1_2OrLater);
    mData->socket.setSslConfiguration(conf);

    connect(&mData->socket, &QWebSocket::textFrameReceived, this,
            [this](const QString &message) {
                QJsonParseError err;
                const auto doc = QJsonDocument::fromJson(message.toUtf8(), &err);
                if (err.error != QJsonParseError::NoError) {
                    Q_EMIT error(u"Failed to parse RTM JSON response: %1"_qs.arg(err.errorString()));
                    return;
                }

                qDebug() << doc;

                try {
                    auto event = SlackAPI::RTM::parseEvent(doc.object());
                    {
                        std::scoped_lock lock{mQueueLock};
                        mEventQueue.emplace_back(std::move(event));
                    }
                    Q_EMIT eventsAvailable();
                } catch (const SlackAPI::RTM::EventException &e) {
                    Q_EMIT error(u"Error when parsing RTM event: %1"_qs.arg(QString::fromUtf8(e.what())));
                }
            });
    connect(&mData->socket, &QWebSocket::stateChanged, this,
            [this](QAbstractSocket::SocketState state) {
                switch (state) {
                case QAbstractSocket::ConnectedState:
                    Q_EMIT connected();
                    break;
                case QAbstractSocket::UnconnectedState:
                    Q_EMIT connectionLost();
                    break;
                default:
                    break;
                }
            });
    connect(&mData->socket, qOverload<QAbstractSocket::SocketError>(&QWebSocket::error), this,
            [this](QAbstractSocket::SocketError) {
                Q_EMIT error(mData->socket.errorString());
            });
    connect(&mData->socket, &QWebSocket::sslErrors, this,
            [this](const QList<QSslError> &errors) {
                for (const auto &sslError: errors) {
                    Q_EMIT error(u"SSL error: "_qs + sslError.errorString());
                }
            });

    connect(&mData->pingTimer, &QTimer::timeout, &mData->socket, [this]() { mData->socket.ping(); });

    mData->socket.open(url);
    mData->pingTimer.start(15s);
}


RTMSocket::~RTMSocket() = default;

void RTMSocket::quit()
{
    QMetaObject::invokeMethod(this, [this]() {
        mData->socket.close();
    }, Qt::QueuedConnection);
}

std::unique_ptr<SlackAPI::RTM::Event> RTMSocket::retrieveEvent()
{
    std::scoped_lock lock{mQueueLock};
    auto event = std::move(mEventQueue.front());
    mEventQueue.pop_front();

    return event;
}

void RTMSocket::sendMessage(QJsonDocument doc)
{
    QMetaObject::invokeMethod(this, [this, doc = std::move(doc)]() {
        const auto data = doc.toJson(QJsonDocument::Compact);
        qDebug() << doc;
        mData->socket.sendTextMessage(QString::fromUtf8(data));
    }, Qt::QueuedConnection);
}
