// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "core/teamcontroller.h"

#include <QAbstractListModel>

#include <memory>

namespace Shirk::Core {

class Config;

class TeamsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        TeamRole = Qt::UserRole + 1,
        ControllerRole
    };

    ~TeamsModel() override;

    void loadControllers(Config &config);

    TeamController &controllerForIndex(const QModelIndex &index) const;
    TeamController &controllerForTeam(const Team &team) const;
    TeamController &controllerForTeam(QStringView teamId) const;
    void addTeamController(std::unique_ptr<TeamController> controller, Config &config);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role) const override;

Q_SIGNALS:
    void teamAdded(Shirk::Core::TeamController *team);

private:
    std::vector<std::unique_ptr<TeamController>> mControllers;

};

} // namespace
