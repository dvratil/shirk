// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>
#include <QWebSocket>
#include <QTimer>

#include <memory>
#include <deque>
#include <mutex>

namespace Shirk::SlackAPI::RTM
{
struct Event;
}

namespace Shirk::Core
{

class RTMSocket : public QObject
{
    Q_OBJECT
public:
    RTMSocket(const QUrl &url);
    ~RTMSocket() override;

    void quit();

    std::unique_ptr<SlackAPI::RTM::Event> retrieveEvent();
    void sendMessage(QJsonDocument msg);

Q_SIGNALS:
    void eventsAvailable();
    void connected();
    void connectionLost();

    void error(const QString &error);

private:
    void init(const QUrl &url);

    struct Data {
        QWebSocket socket;
        QTimer pingTimer;
    };

    std::unique_ptr<Data> mData;

    std::deque<std::unique_ptr<SlackAPI::RTM::Event>> mEventQueue;
    std::mutex mQueueLock;
};


struct RTMSocketDeleter {
    void operator()(RTMSocket *socket);
};
using RTMSocketPtr = std::unique_ptr<RTMSocket, RTMSocketDeleter>;

} // namespace
