// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "conversation.h"
#include "events.h"
#include "future.h"
#include "rtmeventlistener.h"

#include <QAbstractItemModel>

namespace Shirk::Core
{

class TeamController;

class ConversationsModel : public QAbstractItemModel
                         , public RTMEventListener
{
    Q_OBJECT

public:
    ConversationsModel(TeamController &controller);

    Future<void> populate();

    enum Roles {
        ConversationRole = Qt::UserRole + 1,
        GroupRole,
        IsMemberRole
    };

    enum class Group {
        Starred,
        Channels,
        DirectChats
    };
    Q_ENUM(Group)


    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    QHash<int, QByteArray> roleNames() const override;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    QStringList getPresenceIds() const;

protected:
    void rtmMemberJoinedChannelEvent(const SlackAPI::RTM::MemberJoinedChannelEvent &event) override;
    void rtmChannelLeftEvent(const SlackAPI::RTM::ChannelLeftEvent &event) override;

private:
    Group groupForConversation(const Conversation *conv) const;

    TeamController &mController;

    std::vector<std::unique_ptr<Conversation>> mEntries;
};

} // namespace
