// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "common.h"
#include "common_p.h"

#include <QJsonObject>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

bool Response::parseBase(const QJsonValue &value)
{
    const auto obj = value.toObject();

    ok = obj["ok"_ql1].toBool();
    if (!ok) {
        error = obj["error"_ql1].toString();
        return false;
    }

    if (obj.contains("response_metadata"_ql1)) {
        response_metadata = ResponseMetadata{};
        const auto md = obj["response_metadata"_ql1].toObject();
        if (md.contains("next_cursor"_ql1)) {
            response_metadata->next_cursor = md["next_cursor"_ql1].toString();
        }
    }

    return true;
}
