// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "emoji.h"
#include "common_p.h"

#include <algorithm>

#include <QUrlQuery>
#include <QJsonObject>
#include <QVariant>
#include <iterator>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

QUrlQuery EmojiListRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"include_categories"_qs, include_categories ? u"true"_qs : u"false"_qs);
    return query;
}

EmojiListResponse EmojiListResponse::parse(const QJsonValue &value)
{
    EmojiListResponse resp;
    if (!resp.parseBase(value)) {
        return resp;
    }
    const auto emojis = value["emoji"_ql1].toObject();
    for (auto it = emojis.begin(), end = emojis.end(); it != end; ++it) {
        resp.emojis.insert(it.key(), it.value().toString());
    }
    const auto categories = value["categories"_ql1].toArray();
    for (const auto &category : categories) {
        const auto list = category["emoji_names"_ql1].toArray();
        QStringList names;
        names.reserve(list.size());
        std::transform(list.cbegin(), list.cend(), std::back_inserter(names), [](const auto &v) { return v.toString(); });
        resp.categories.insert(category["name"_ql1].toString(), names);
    }

    return resp;
}
