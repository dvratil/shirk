// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "conversations.h"
#include "common_p.h"
#include "utils/stringliterals.h"

#include <QJsonObject>
#include <QJsonValue>
#include <QUrlQuery>

#include <range/v3/algorithm/transform.hpp>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

Conversation Conversation::parse(const QJsonValue &data)
{
    Conversation resp;
    const auto chan = data.toObject();
    API_EXTRACT_STR(resp, chan, id);
    API_EXTRACT_STR(resp, chan, name);
    API_EXTRACT_STR(resp, chan, user);
    API_EXTRACT_STR(resp, chan, name_normalized);
    API_EXTRACT_STRLIST(resp, chan, previous_names);
    API_EXTRACT_STR(resp, chan, creator);
    API_EXTRACT_STR(resp, chan, locale);
    API_EXTRACT_STRLIST(resp, chan, pending_shared);
    API_EXTRACT_INT(resp, chan, num_members);

    API_EXTRACT_INT(resp, chan, last_read);
    API_EXTRACT_INT(resp, chan, created);
    API_EXTRACT_INT(resp, chan, unread_count);
    API_EXTRACT_INT(resp, chan, unread_count_display);
    API_EXTRACT_INT(resp, chan, latest);
    API_EXTRACT_BOOL(resp, chan, is_member);

    API_EXTRACT_BOOL(resp, chan, is_archived);
    API_EXTRACT_BOOL(resp, chan, is_channel);
    API_EXTRACT_BOOL(resp, chan, is_general);
    API_EXTRACT_BOOL(resp, chan, is_group);
    API_EXTRACT_BOOL(resp, chan, is_im);
    API_EXTRACT_BOOL(resp, chan, is_mpim);
    API_EXTRACT_BOOL(resp, chan, is_private);

    API_EXTRACT_BOOL(resp, chan, is_shared);
    API_EXTRACT_BOOL(resp, chan, is_ext_shared);
    API_EXTRACT_BOOL(resp, chan, is_pending_ext_shared);

    const auto topic = data["topic"_ql1].toObject();
    API_EXTRACT_STR(resp.topic, topic, value);
    API_EXTRACT_STR(resp.topic, topic, creator);
    API_EXTRACT_INT(resp.topic, topic, last_set);

    const auto purpose = data["purpose"_ql1].toObject();
    API_EXTRACT_STR(resp.purpose, purpose, value);
    API_EXTRACT_STR(resp.purpose, purpose, creator);
    API_EXTRACT_INT(resp.purpose, purpose, last_set);

    return resp;
}

QUrlQuery ConversationsListRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"cursor"_qs, cursor);
    query.addQueryItem(u"exclude_archived"_qs, bool2str(exclude_archived));
    query.addQueryItem(u"limit"_qs, QString::number(limit));
    QStringList type_str;
    for (auto type : types) {
        switch (type) {
        case Type::PublicChannel:
            type_str.push_back(u"public_channel"_qs);
            break;
        case Type::PrivateChannel:
            type_str.push_back(u"private_channel"_qs);
            break;
        case Type::IM:
            type_str.push_back(u"im"_qs);
            break;
        case Type::MPIM:
            type_str.push_back(u"mpim"_qs);
            break;
        }
    }
    query.addQueryItem(u"types"_qs, type_str.join(QLatin1Char(',')));

    return query;
}

ConversationsListResponse ConversationsListResponse::parse(const QJsonValue &value)
{
    ConversationsListResponse resp;
    if (!resp.parseBase(value)) {
        return resp;
    }
    const auto convs = value["channels"_ql1].toArray();
    for (const auto &conv : convs) {
        resp.channels.push_back(Conversation::parse(conv));
    }

    return resp;
}

QUrlQuery ConversationHistoryRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"channel"_qs, channel);
    query.addQueryItem(u"cursor"_qs, cursor);
    query.addQueryItem(u"inclusive"_qs, inclusive ? u"1"_qs : u"0"_qs);
    if (latest.has_value()) {
        query.addQueryItem(u"latest"_qs, *latest);
    }
    query.addQueryItem(u"limit"_qs, QString::number(limit));
    query.addQueryItem(u"oldest"_qs, oldest.has_value() ? oldest->value() : u"0"_qs);
    return query;
}

ConversationHistoryResponse ConversationHistoryResponse::parse(const QJsonValue &value)
{
    ConversationHistoryResponse resp;
    if (!resp.parseBase(value)) {
        return resp;
    }
    API_EXTRACT_TSID(resp, value, latest);
    API_EXTRACT_BOOL(resp, value, has_more);
    API_EXTRACT_INT(resp, value, pin_count);
    ranges::transform(value[u"messages"].toArray(), std::back_inserter(resp.messages), [](const auto &m) { return Message{m}; });

    return resp;
}

QUrlQuery ConversationJoinRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"channel"_qs, channel);
    return query;
}

ConversationJoinResponse ConversationJoinResponse::parse(const QJsonValue &value)
{
    ConversationJoinResponse resp;
    if (!resp.parseBase(value)) {
        return resp;
    }
    resp.conversation = Conversation::parse(value["channel"_ql1]);
    return resp;
}

QUrlQuery ConversationLeaveRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"channel"_qs, channel);
    return query;
}

ConversationLeaveResponse ConversationLeaveResponse::parse(const QJsonValue &value)
{
    ConversationLeaveResponse resp;
    resp.parseBase(value);
    return resp;
}

