// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <optional>
#include <QString>

class QJsonValue;

namespace Shirk::SlackAPI {

class UnauthenticatedRequest {};
class UserAuthenticatedRequest {};
class BotAuthenticatedRequest {};
class RTMRequest {};

class Response {
public:
    bool ok = false;
    std::optional<QString> error;

    struct ResponseMetadata {
        std::optional<QString> next_cursor;
    };
    std::optional<ResponseMetadata> response_metadata;

protected:
    bool parseBase(const QJsonValue &value);
};

enum class Method {
    GET,
    POST,
    PUT,
    DELETE
};

} // namespace
