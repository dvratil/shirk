// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "message.h"
#include "common_p.h"
#include "utils/stringliterals.h"

#include <QJsonValue>

#include <range/v3/view/transform.hpp>
#include <range/v3/range/conversion.hpp>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;
using namespace ranges;

Attachment::Attachment(const QJsonValue &value)
    : CTOR_EXTRACT_INT(value, id)
    , CTOR_EXTRACT_STR(value, service_name)
    , CTOR_EXTRACT_STR(value, text)
    , CTOR_EXTRACT_STR(value, fallback)
    , CTOR_EXTRACT_URL(value, thumb_url)
    , CTOR_EXTRACT_INT(value, thumb_width)
    , CTOR_EXTRACT_INT(value, thumb_height)
{}

Reaction::Reaction(const QJsonValue &value)
    : CTOR_EXTRACT_STR(value, name)
    , CTOR_EXTRACT_STRLIST(value, users)
    , CTOR_EXTRACT_INT(value, count)
{}

Message::Message(const QJsonValue &value)
    : CTOR_EXTRACT_STR(value, user)
    , CTOR_EXTRACT_STR(value, text)
    , CTOR_EXTRACT_TSID(value, ts)
    , CTOR_EXTRACT_TSID(value, thread_ts)
    , CTOR_EXTRACT_INT(value, reply_count)
    , CTOR_EXTRACT_STRLIST(value, reply_users)
    , CTOR_EXTRACT_TSID(value, latest_reply)
{
    const auto attachmentsArr = value[u"attachments"].toArray();
    attachments = attachmentsArr | view::transform([](const auto &a) { return Attachment{a}; }) | to<QVector>();
    const auto recationsArr = value[u"recations"].toArray();
    reactions = recationsArr | view::transform([](const auto &r) { return Reaction{r}; }) | to<QVector>();
}
