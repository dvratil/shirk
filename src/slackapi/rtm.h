// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "common.h"

#include <QStringView>
#include <QUrl>
#include <QStringList>

class QUrlQuery;
class QJsonValue;
class QJsonDocument;

namespace Shirk::SlackAPI
{

struct RTMConnectRequest : public UserAuthenticatedRequest
{
    static constexpr QStringView endpoint = u"rtm.connect";
    static constexpr Method method = Method::GET;

    QUrlQuery serialize() const;
};

struct RTMConnectResponse : public Response
{
    QUrl url;

    static RTMConnectResponse parse(const QJsonValue &value);
};

struct PresenceSubscriptionRequest : public RTMRequest
{
    QStringList ids;

    QJsonDocument serialize() const;
};

} // namespace
