// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "rtm.h"
#include "common_p.h"

#include <QUrlQuery>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

QUrlQuery RTMConnectRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"batch_presence_aware"_qs, u"1"_qs);
    query.addQueryItem(u"presense_sub"_qs, u"1"_qs);
    return query;
}

RTMConnectResponse RTMConnectResponse::parse(const QJsonValue &value)
{
    RTMConnectResponse resp;
    API_EXTRACT_URL(resp, value, url);
    return resp;
}

QJsonDocument PresenceSubscriptionRequest::serialize() const
{
    return QJsonDocument{{
        {u"type"_qs, u"presence_sub"_qs},
        {u"ids"_qs, QJsonArray::fromStringList(ids)}
    }};
}
