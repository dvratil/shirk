// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "common.h"

#include <QUrlQuery>
#include <QUrl>
#include <QMap>

class QJsonValue;

namespace Shirk::SlackAPI
{

class EmojiListRequest : public UserAuthenticatedRequest
{
public:
    static constexpr Method method = Method::GET;
    static constexpr QStringView endpoint = u"emoji.list";

    bool include_categories = false;

    QUrlQuery serialize() const;
};

class EmojiListResponse : public Response
{
public:
    QMap<QString /*name*/, QString /*url*/> emojis;
    QMap<QString /*category name*/, QStringList /*emoji names*/> categories;

    static EmojiListResponse parse(const QJsonValue &val);
};


}
