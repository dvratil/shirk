// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "files.h"
#include "common_p.h"
#include "utils/stringliterals.h"

#include <QJsonValue>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

File::File(const QJsonValue &value)
    : CTOR_EXTRACT_STR(value, id)
    , CTOR_EXTRACT_UINT(value, created)
    , CTOR_EXTRACT_UINT(value, updated)
    , CTOR_EXTRACT_STR(value, name)
    , CTOR_EXTRACT_STR(value, title)
    , CTOR_EXTRACT_STR(value, mimetype)
    , CTOR_EXTRACT_STR(value, filetype)
    , CTOR_EXTRACT_STR(value, pretty_type)
    , CTOR_EXTRACT_STR(value, user)
    , CTOR_EXTRACT_BOOL(value, editable)
    , CTOR_EXTRACT_UINT(value, size)
    , CTOR_EXTRACT_STR(value, mode)
    , CTOR_EXTRACT_BOOL(value, is_external)
    , CTOR_EXTRACT_STR(value, external_type)
    , CTOR_EXTRACT_BOOL(value, is_public)
    , CTOR_EXTRACT_BOOL(value, public_url_shared)
    , CTOR_EXTRACT_BOOL(value, display_as_bot)
    , CTOR_EXTRACT_STR(value, username)
    , CTOR_EXTRACT_STR(value, preview)
    , CTOR_EXTRACT_STR(value, preview_highlight)
    , CTOR_EXTRACT_INT(value, lines)
    , CTOR_EXTRACT_INT(value, lines_more)
    , CTOR_EXTRACT_URL(value, url_private)
    , CTOR_EXTRACT_URL(value, url_private_download)
    , CTOR_EXTRACT_URL(value, thumb_64)
    , CTOR_EXTRACT_URL(value, thumb_88)
    , CTOR_EXTRACT_URL(value, thumb_360)
    , CTOR_EXTRACT_URL(value, thumb_360_w)
    , CTOR_EXTRACT_URL(value, thumb_360_h)
    , CTOR_EXTRACT_URL(value, thumb_160)
    , CTOR_EXTRACT_URL(value, thumb_360_gif)
    , CTOR_EXTRACT_URL(value, thumb_480)
    , CTOR_EXTRACT_URL(value, thumb_720)
    , CTOR_EXTRACT_URL(value, thumb_960)
    , CTOR_EXTRACT_URL(value, thumb_1024)
    , CTOR_EXTRACT_INT(value, image_exif_rotation)
    , CTOR_EXTRACT_INT(value, original_w)
    , CTOR_EXTRACT_INT(value, original_h)
    , CTOR_EXTRACT_URL(value, deanimate_gif)
    , CTOR_EXTRACT_URL(value, pjpeg)
    , CTOR_EXTRACT_URL(value, permalink)
    , CTOR_EXTRACT_URL(value, permalink_public)
    , CTOR_EXTRACT_STR(value, initial_comment)
    , CTOR_EXTRACT_INT(value, comments_count)
    , CTOR_EXTRACT_INT(value, num_stars)
    , CTOR_EXTRACT_BOOL(value, is_starred)
    , CTOR_EXTRACT_STRLIST(value, pinned_to)
    , CTOR_EXTRACT_STRLIST(value, channels)
    , CTOR_EXTRACT_STRLIST(value, groups)
    , CTOR_EXTRACT_STRLIST(value, ims)
    , CTOR_EXTRACT_BOOL(value, has_rich_preview)
{}
