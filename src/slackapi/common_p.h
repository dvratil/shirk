// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "utils/stringliterals.h"

#include <QJsonArray>
#include <QStringList>

namespace Shirk::SlackAPI
{

inline QString bool2str(bool val)
{
    using namespace Shirk::StringLiterals;

    return val ? u"true"_qs : u"false"_qs;
}

inline QStringList toStringList(const QJsonValue &val)
{
    if (val.isNull() || val.isUndefined()) {
        return {};
    }
    Q_ASSERT(val.isArray());
    const auto arr = val.toArray();
    QStringList rv;
    rv.reserve(arr.size());
    std::transform(arr.cbegin(), arr.cend(), std::back_inserter(rv), [](const auto &val) {
        return val.toString();
    });
    return rv;
}

#define API_EXTRACT_STR(resp, val, name)    resp.name = val[#name ## _ql1].toString()
#define API_EXTRACT_BOOL(resp, val, name)   resp.name = val[#name ## _ql1].toBool(false)
#define API_EXTRACT_INT(resp, val, name)    resp.name = val[#name ## _ql1].toInt()
#define API_EXTRACT_STRLIST(resp, val, name)    resp.name = toStringList(val[#name ## _ql1])
#define API_EXTRACT_URL(resp, val, name)    resp.name = QUrl(val[#name ## _ql1].toString())
#define API_EXTRACT_TSID(resp, val, name)   API_EXTRACT_STR(resp, val, name)
#define API_EXTRACT_DATETIME(resp, val, name)   resp.name = QDateTime::fromSecsSinceEpoch(val[#name ## _ql1].toInt())

#define CTOR_EXTRACT_STR(val, name)         name{val[u## #name].toString()}
#define CTOR_EXTRACT_BOOL(val, name)        name{val[u## #name].toBool(false)}
#define CTOR_EXTRACT_INT(val, name)         name{val[u## #name].toInt()}
#define CTOR_EXTRACT_UINT(val, name)        name{static_cast<uint32_t>(val[u## #name].toInt())}
#define CTOR_EXTRACT_STRLIST(val, name)     name{toStringList(val[u## #name])}
#define CTOR_EXTRACT_URL(val, name)         CTOR_EXTRACT_STR(val, name)
#define CTOR_EXTRACT_TSID(val, name)        CTOR_EXTRACT_STR(val, name)
#define CTOR_EXTRACT_DATETIME(val, name)    name{QDateTime::fromSecsSinceEpoch(val[u## #name].toInt())}

} // namespace
