// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "events.h"
#include "common_p.h"
#include "utils/stringliterals.h"

#include <QJsonValue>
#include <QJsonObject>
#include <QStringView>
#include <QMetaObject>
#include <QMetaEnum>
#include <QDebug>

#include <algorithm>
#include <type_traits>

using namespace Shirk::SlackAPI::RTM;
using namespace Shirk::StringLiterals;

class Shirk::SlackAPI::RTM::EventFactory
{
public:
    template<typename T>
    static std::unique_ptr<Event> build(const QJsonValue &val)
    {
        return std::unique_ptr<Event>{new T{val}};
    }

    template<typename T, typename ... Args>
    static std::unique_ptr<Event> buildWithArgs(Args && ... args)
    {
        return std::unique_ptr<Event>{new T{std::forward<Args>(args) ...}};
    }
};

namespace {

struct Entry {
    using MakeFunc = std::unique_ptr<Event>(*)(const QJsonValue &);

    constexpr Entry(QStringView name, MakeFunc func)
        : name(name), makeFunc(func)
    {}

    const QStringView name;
    const MakeFunc makeFunc;
};

template<typename ... T>
constexpr std::array<Entry, sizeof...(T)> makeEventRegistry()
{
    return {Entry{T::eventName, &EventFactory::build<T>} ...};
}

constexpr const auto eventRegistry = makeEventRegistry<
        HelloEvent,
        MessageEvent,
        MemberJoinedChannelEvent,
        ChannelLeftEvent,
        ReactionAddedEvent
    >();

constexpr const auto messageEventRegistry = makeEventRegistry<
        BotMessageEvent,
        ChannelArchiveMessageEvent,
        ChannelJoinMessageEvent,
        ChannelLeaveMessageEvent,
        ChannelNameMessageEvent,
        ChannelPurposeMessageEvent,
        ChannelTopicMessageEvent,
        ChannelUnarchiveMessageEvent,
        EKMAccessDeniedMessageEvent,
        FileCommentMessageEvent,
        FileMentionMessageEvent,
        GroupArchiveMessageEvent,
        GroupJoinMessageEvent,
        GroupLeaveMessageEvent,
        GroupNameMessageEvent,
        GroupPurposeMessageEvent,
        GroupTopicMessageEvent,
        GroupUnarchiveMessageEvent,
        MeMessageEvent,
        MessageChangedEvent,
        MessageDeletedEvent
    >();

template<typename Registry>
auto findInRegistry(const Registry &registry, QStringView type)
{
    return std::find_if(registry.cbegin(), registry.cend(), [type](const auto &v) {
        return v.name == type;
    });
}

} // namespace

QDebug operator<<(QDebug debug, EventType type)
{
    constexpr auto &mo = Shirk::SlackAPI::RTM::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("Type");
    Q_ASSERT(idx > -1);
    const auto enumerator = mo.enumerator(idx);
    return debug.noquote() << enumerator.valueToKey(static_cast<int>(type));
}


std::unique_ptr<Event> Shirk::SlackAPI::RTM::parseEvent(const QJsonValue &value)
{
    const auto construct = [](auto &&registry, auto &&key, const auto &value) {
        const auto entry = findInRegistry(registry, key);
        if (entry == registry.cend()) {
            throw EventException(u"Unsupported type '%1'"_qs.arg(key));
        }

        return entry->makeFunc(value);
    };


    const auto type = value["type"_ql1].toString();
    if (type == MessageEvent::eventName) {
        if (!value[u"subtype"].isUndefined()) {
            return construct(messageEventRegistry, value[u"subtype"].toString(), value);
        }
        return EventFactory::buildWithArgs<MessageEvent>(MessageEvent::SubType::Message, value);
    }

    return construct(eventRegistry, type, value);
}



#define EVENT_CTOR(type_, ...) \
type_##Event::type_##Event([[maybe_unused]] const QJsonValue &value) \
    : Event(&type_##Event::type) \
    __VA_OPT__(,) __VA_ARGS__ \
{}

EVENT_CTOR(Hello)

MessageEvent::MessageEvent(SubType subType, const QJsonValue &value)
    : Event(&MessageEvent::type)
    , CTOR_EXTRACT_STR(value, channel)
    , CTOR_EXTRACT_STR(value, user)
    , CTOR_EXTRACT_STR(value, text)
    , CTOR_EXTRACT_TSID(value, ts)
    , subType(subType)
{
    if (const auto hidden = value["hidden"_ql1]; hidden.type() == QJsonValue::Bool) {
        this->hidden = hidden.toBool();
    }
}

#define MESSAGEEVENT_CTOR(klass, subtype, ...) \
klass##Event::klass##Event(const QJsonValue &value) \
    : MessageEvent(SubType::subtype, value) \
    __VA_OPT__(,) __VA_ARGS__ \
{}

MESSAGEEVENT_CTOR(BotMessage, BotMessage
    , CTOR_EXTRACT_STR(value, bot_id)
    , CTOR_EXTRACT_STR(value, username)
    , CTOR_EXTRACT_STRLIST(value, icons)
)

MESSAGEEVENT_CTOR(ChannelArchiveMessage, ChannelArchive)
MESSAGEEVENT_CTOR(ChannelJoinMessage, ChannelJoin
    , CTOR_EXTRACT_STR(value, inviter)
)
MESSAGEEVENT_CTOR(ChannelLeaveMessage, ChannelLeave)
MESSAGEEVENT_CTOR(ChannelNameMessage, ChannelName
    , CTOR_EXTRACT_STR(value, old_name)
    , CTOR_EXTRACT_STR(value, name)
)
MESSAGEEVENT_CTOR(ChannelPurposeMessage, ChannelPurpose
    , CTOR_EXTRACT_STR(value, purpose)
)
MESSAGEEVENT_CTOR(ChannelTopicMessage, ChannelTopic
    , CTOR_EXTRACT_STR(value, topic)
)
MESSAGEEVENT_CTOR(ChannelUnarchiveMessage, ChannelUnarchive)
MESSAGEEVENT_CTOR(EKMAccessDeniedMessage, EKMAccessDenied)
MESSAGEEVENT_CTOR(FileCommentMessage, FileComment
    , file(value[u"file"])
)
MESSAGEEVENT_CTOR(FileMentionMessage, FileMention
    , file(value[u"file"])
)
MESSAGEEVENT_CTOR(GroupArchiveMessage, GroupArchive
    , CTOR_EXTRACT_STRLIST(value, members)
)
MESSAGEEVENT_CTOR(GroupJoinMessage, GroupJoin)
MESSAGEEVENT_CTOR(GroupLeaveMessage, GroupLeave)
MESSAGEEVENT_CTOR(GroupNameMessage, GroupName
    , CTOR_EXTRACT_STR(value, old_name)
    , CTOR_EXTRACT_STR(value, name)
)
MESSAGEEVENT_CTOR(GroupPurposeMessage, GroupPurpose
    , CTOR_EXTRACT_STR(value, purpose)
)
MESSAGEEVENT_CTOR(GroupTopicMessage, GroupTopic
    , CTOR_EXTRACT_STR(value, topic)
)
MESSAGEEVENT_CTOR(GroupUnarchiveMessage, GroupUnarchive)
MESSAGEEVENT_CTOR(MeMessage, MeMessage)
MESSAGEEVENT_CTOR(MessageChanged, MessageChanged
    , edited_user(value[u"message"][u"edited"][u"user"].toString())
    , edited_ts(value[u"message"][u"edited"][u"ts"].toString())
)
MESSAGEEVENT_CTOR(MessageDeleted, MessageDeleted
    , CTOR_EXTRACT_TSID(value, deleted_ts)
)

EVENT_CTOR(MemberJoinedChannel
    , CTOR_EXTRACT_STR(value, user)
    , CTOR_EXTRACT_STR(value, team)
    , CTOR_EXTRACT_STR(value, channel)
    , CTOR_EXTRACT_STR(value, channel_type)
    , CTOR_EXTRACT_STR(value, inviter)
)

EVENT_CTOR(ChannelLeft
    , CTOR_EXTRACT_STR(value, actor_id)
    , CTOR_EXTRACT_STR(value, channel)
)

ReactionAddedEvent::Item::Item(const QJsonValue &value)
    : CTOR_EXTRACT_STR(value, channel)
    , CTOR_EXTRACT_TSID(value, ts)
    , CTOR_EXTRACT_STR(value, type)
{}

EVENT_CTOR(ReactionAdded
    , item{value[u"item"]}
    , CTOR_EXTRACT_STR(value, item_user)
    , CTOR_EXTRACT_STR(value, reaction)
    , CTOR_EXTRACT_TSID(value, ts)
)

