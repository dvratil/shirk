#pragma once

#include <compare>

#include <QString>
#include <QDateTime>

namespace Shirk::SlackAPI
{

struct TSID
{
    TSID(const QString &value)
        : mValue(value)
    {}

    auto operator<=>(const TSID &other) const {
        return toInt() <=> other.toInt();
    }

    operator QString() const
    {
        return mValue;
    }

    QString value() const
    {
        return *this;
    }

    QDateTime toTimestamp() const
    {
        return QDateTime::fromMSecsSinceEpoch(mValue.toDouble() * 1'000);
    }

private:
    qint64 toInt() const
    {
        return mValue.toDouble() * 10'000;
    }

    QString mValue;
};

}
