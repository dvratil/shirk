// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "common.h"
#include "message.h"

#include <QString>
#include <QUrl>
#include <QUrlQuery>

#include <cmath>

class QJsonValue;

namespace Shirk::SlackAPI {

class ConversationsListRequest : public UserAuthenticatedRequest
{
public:
    static constexpr const Method method = Method::GET;
    static constexpr const QStringView endpoint = u"conversations.list";

    enum class Type {
        PublicChannel,
        PrivateChannel,
        MPIM,
        IM
    };

    QString cursor = {};
    bool exclude_archived = false;
    int limit = 100;
    QVector<Type> types = {Type::PublicChannel};

    QUrlQuery serialize() const;
};

struct Conversation
{
    QString id;
    QString name; // when is_channel/is_group == true
    QString user; // when is_im == true
    QString name_normalized;
    QStringList previous_names;
    QString creator;
    QString locale;
    QStringList pending_shared;
    int num_members;

    uint32_t last_read = 0;
    uint32_t created = 0;
    uint32_t unread_count = 0;
    uint32_t unread_count_display = 0;
    uint32_t latest = 0;
    bool is_member = true;

    bool is_archived = false;
    bool is_read_only = false;

    bool is_general = false;
    bool is_channel = false;
    bool is_group = false;
    bool is_im = false;
    bool is_mpim = false;
    bool is_private = false;

    bool is_shared = false;
    bool is_ext_shared = false;
    bool is_org_shared = false;
    bool is_pending_ext_shared = false;
    struct {
        QString value;
        QString creator;
        uint32_t last_set;
    } topic;
    struct {
        QString value;
        QString creator;
        uint32_t last_set;
    } purpose;

    static Conversation parse(const QJsonValue &data);
};


class ConversationsListResponse : public Response
{
public:
    QVector<Conversation> channels;

    static ConversationsListResponse parse(const QJsonValue &data);
};

class ConversationHistoryRequest : public UserAuthenticatedRequest
{
public:
    static constexpr const Method method = Method::GET;
    static constexpr const QStringView endpoint = u"conversations.history";

    QString cursor = {};
    QString channel = {};
    bool inclusive = true;
    std::optional<TSID> latest;
    std::optional<TSID> oldest;
    int limit = 20;

    QUrlQuery serialize() const;
};

class ConversationHistoryResponse : public Response
{
public:
    std::optional<TSID> latest;
    bool has_more = false;
    int pin_count = 0;
    QVector<Message> messages;

    static ConversationHistoryResponse parse(const QJsonValue &data);
};

class ConversationJoinRequest : public UserAuthenticatedRequest
{
public:
    static constexpr const Method method = Method::GET;
    static constexpr const QStringView endpoint = u"conversations.join";

    QString channel;

    QUrlQuery serialize() const;
};

class ConversationJoinResponse : public Response
{
public:
    Conversation conversation;

    static ConversationJoinResponse parse(const QJsonValue &value);
};

class ConversationLeaveRequest : public UserAuthenticatedRequest
{
public:
    static constexpr const Method method = Method::GET;
    static constexpr const QStringView endpoint = u"conversations.leave";

    QString channel;

    QUrlQuery serialize() const;
};

class ConversationLeaveResponse : public Response
{
public:
    static ConversationLeaveResponse parse(const QJsonValue &value);
};

} // namespace
