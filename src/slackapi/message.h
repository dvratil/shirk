// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "tsid.h"

#include <QString>
#include <QVector>
#include <QUrl>

class QJsonValue;

namespace Shirk::SlackAPI
{

struct Attachment
{
    explicit Attachment(const QJsonValue &value);

    int id = 0;
    QString service_name;
    QString text;
    QString fallback;
    QUrl thumb_url;
    int thumb_width = 0;
    int thumb_height = 0;
};

struct Reaction
{
    explicit Reaction(const QJsonValue &value);

    QString name;
    QStringList users;
    int count = 0;
};

struct Message
{
    explicit Message(const QJsonValue &value);

    QString user;
    QString text;
    TSID ts;
    TSID thread_ts;
    int reply_count = 0;
    QStringList reply_users;
    TSID latest_reply;
    QVector<Attachment> attachments;
    QVector<Reaction> reactions;
};

} // namespace
