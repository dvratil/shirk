// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "oauth.h"
#include "common_p.h"

#include <QJsonObject>
#include <QJsonValue>
#include <QUrlQuery>

using namespace Shirk::SlackAPI;
using namespace Shirk::StringLiterals;

QUrlQuery OAuthAccessRequest::serialize() const
{
    QUrlQuery query;
    query.addQueryItem(u"client_id"_qs, clientId);
    query.addQueryItem(u"client_secret"_qs, clientSecret);
    query.addQueryItem(u"code"_qs, code);
    query.addQueryItem(u"redirect_uri"_qs, redirectUri.toString());
    return query;
}

OAuthAccessResponse OAuthAccessResponse::parse(const QJsonValue &data)
{
    OAuthAccessResponse resp;
    if (!resp.parseBase(data)) {
        return resp;
    }

    API_EXTRACT_STR(resp, data, access_token);
    API_EXTRACT_STR(resp, data, scope);
    API_EXTRACT_STR(resp, data, team_name);
    API_EXTRACT_STR(resp, data, team_id);
    return resp;
}
