// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "gui/application.h"

#include <KDBusService>

using namespace Shirk;

int main(int argc, char **argv)
{
    qputenv("QT_QUICK_CONTROLS_STYLE", "org.kde.desktop");

    Q_INIT_RESOURCE(emoji);

    Gui::Application app(argc, argv);

    KDBusService uniqueApp(KDBusService::Unique);
    QObject::connect(&uniqueApp, &KDBusService::activateRequested, &app, &Gui::Application::raiseMainWindow);

    return app.exec();
}
