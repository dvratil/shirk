// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "iconprovider.h"
#include "core/teamsmodel.h"
#include "core/teamcontroller.h"
#include "core/usermanager.h"
#include "gui_debug.h"

using namespace Shirk::Gui;

IconProvider::IconProvider(Core::TeamsModel &model)
    : QQuickImageProvider(QQmlImageProviderBase::Pixmap)
    , mModel(model)
{}

QPixmap IconProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    const auto url = id.splitRef(QLatin1Char('/'));
    if (url.size() != 2) {
        qCWarning(LOG_GUI) << "Invalid icon provider URL" << url;
        return {};
    }

    const auto teamId = url[0];
    const auto userId = url[1];

    auto &userManager = mModel.controllerForTeam(teamId).userManager();
    const auto data = userManager.getUserData(userId);
    if (data.has_value()) {
        const auto rs = requestedSize.isValid() ? requestedSize
                                                : data->get().avatar.availableSizes().last();
        const auto pixmap = data->get().avatar.pixmap(rs);
        *size = pixmap.size();
        return pixmap;
    } else {
        qCWarning(LOG_GUI) << "Unexpected: missing user data for" << userId;
    }

    return {};
}
