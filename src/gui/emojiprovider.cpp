// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "emojiprovider.h"
#include "emojimodel.h"
#include "gui_debug.h"
#include "core/teamcontroller.h"
#include "core/teamsmodel.h"
#include "utils/stringliterals.h"
#include "emoji/compiled_emoji.h"
#include "core/iconloader.h"

#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/algorithm/find.hpp>

#include <QQuickImageResponse>
#include <QImage>
#include <QImageReader>
#include <QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QBuffer>
#include <QDir>

using namespace Shirk;
using namespace Shirk::Gui;
using namespace Shirk::StringLiterals;
using namespace std::chrono_literals;

class AtlasRequest : public QQuickImageResponse
{
    Q_OBJECT
public:
    AtlasRequest(const QImage &sheet, const QString &id, const QSize &)
    {
        connect(&mWatcher, &QFutureWatcher<QImage>::finished,
                this, &AtlasRequest::finished);

        auto future = QtConcurrent::run([&sheet, id]() {
            auto emoji = ranges::find_if(Emoji::s_emojis, [id](const auto &emoji) {
                const auto aliases = emoji.aliases();
                return emoji.name() == id || ranges::find(aliases, id) != aliases.end();
            });
            if (emoji == Emoji::s_emojis.end()) {
                qCWarning(LOG_GUI) << "Couldn't find emoji" << id << "in the default emoji sheet";
                return QImage{};
            }

            const int x = emoji->sheetX() * emojiWidth;
            const int y = emoji->sheetY() * emojiHeight;
            return sheet.copy(x, y, emojiWidth, emojiHeight);
        });
        mWatcher.setFuture(future);
    }

    void cancel() override
    {
        mWatcher.cancel();
        Q_EMIT finished();
    }

    QQuickTextureFactory *textureFactory() const override
    {
        // TODO: Use custom texture factory that uses the sheet as an atlass,
        // this would allow to us to make the request much faster and memory
        // efficient
        return QQuickTextureFactory::textureFactoryForImage(mWatcher.result());
    }

private:
    QFutureWatcher<QImage> mWatcher;

    static constexpr const int emojiWidth = 22;
    static constexpr const int emojiHeight = 22;
};

class RemoteRequest : public QQuickImageResponse
{
    Q_OBJECT
public:
    RemoteRequest(const QString &id, const QUrl &url, const QSize &)
        : QQuickImageResponse()
    {
        const auto file = QStandardPaths::locate(QStandardPaths::CacheLocation, u"emojis/%1"_qsv.arg(id));
        if (file.isNull()) {
            qDebug() << url;
            mReply.reset(mNAM.get(QNetworkRequest{url}));
            connect(mReply.get(), &QNetworkReply::finished,
                    this, [this, id, url]() {
                if (mReply->isOpen()) {
                    const auto formats = QImageReader::imageFormatsForMimeType(mReply->header(QNetworkRequest::ContentTypeHeader).toByteArray());
                    const auto format = formats.empty() ? QByteArray{} : formats.front();
                    auto data = mReply->readAll();
                    QBuffer buffer{&data};
                    QImageReader reader{&buffer, format};
                    mImage = reader.read();
                    if (reader.error() != QImageReader::UnknownError) {
                        qCWarning(LOG_GUI) << "Failed to load emoji" << url << ":" << reader.errorString();
                    } else {
                        cacheEmoji(id, url, data);
                    }
                }
                Q_EMIT finished();
            });
        } else {
            QImageReader reader{file};
            mImage = reader.read();
            if (reader.error() != QImageReader::UnknownError) {
                qCWarning(LOG_GUI) << "Failed to load emoji" << id << "from cache file" << file << ":" << reader.errorString();
                QFile::remove(file);
            }
            QTimer::singleShot(0, this, &RemoteRequest::finished);
        }
    }

    void cancel() override
    {
        if (mReply) {
            mReply->abort();
        }
    }

    QQuickTextureFactory *textureFactory() const override
    {
        return QQuickTextureFactory::textureFactoryForImage(mImage);
    }

private:
    enum CacheDirOpts {
        NoOpts = 0,
        CreateIfNeeded = 1
    };
    QString getCacheDir(CacheDirOpts opts = NoOpts) const
    {
        const QString dir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + u"/emojis/";
        if (opts == CreateIfNeeded && !QDir{dir}.exists()) {
            QDir{}.mkpath(dir);
        }
        return dir;
    }

    void cacheEmoji(const QString &id, const QUrl &url, const QByteArray &data)
    {
        const QString dir = getCacheDir(CreateIfNeeded);
        QFile cacheFile{dir + id};
        if (!cacheFile.open(QIODevice::WriteOnly)) {
            qCWarning(LOG_GUI) << "Failed to cache emoji" << url << "to file" << cacheFile.fileName() << ":" << cacheFile.errorString();
        } else {
            cacheFile.write(data);
            qCDebug(LOG_GUI) << "Caching emoji" << id << "as" << cacheFile.fileName();
        }
    }

    QNetworkAccessManager mNAM;
    QImage mImage;
    std::unique_ptr<QNetworkReply> mReply;
};

EmojiProvider::EmojiProvider(Core::TeamsModel &model)
    : mModel(model)
    , mSheet{u":/emoji/sheet_apple_20.png"_qs, "PNG"}
{}

QQuickImageResponse *EmojiProvider::requestImageResponse(const QString &id, const QSize &requestedSize)
{
    qCDebug(LOG_GUI) << id << requestedSize;
    const auto uri = id.splitRef(QLatin1Char('/'));
    if (uri[0] == u"default"_qsv) {
        return new AtlasRequest{mSheet, uri[1].toString(), requestedSize};
    } else {
        auto &controller = mModel.controllerForTeam(uri[0]);
        return new RemoteRequest{uri[1].toString(), controller.emojiModel().urlForEmoji(uri[1]), requestedSize};
    }
}


#include "emojiprovider.moc"
