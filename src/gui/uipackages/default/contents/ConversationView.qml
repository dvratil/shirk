// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

Kirigami.Page {
    id: root
    property var controller
    property var conversation

    ColumnLayout {
        anchors.fill: parent

        ConversationHeader {
            id: header
            conversation: root.conversation

            Layout.fillWidth: true

            onChannelDetailsRequested: conversationDetailsSheet.open()
            onChannelSettingsRequested: conversationSettingsSheet.open();
        }

        ListView {
            id: scrollView

            Layout.fillWidth: true
            Layout.fillHeight: true

            keyNavigationEnabled: true
            keyNavigationWraps: false

            clip: true
            model: controller.messageModelForConversation(conversation)

            delegate: MessageDelegate {
                controller: root.controller
                message: model.message

                width: parent.width

                onEmojiRequested: emojiDrawer.open()
            }

            onCountChanged: {
                positionTimer.start()
            }

            Timer {
                id: positionTimer
                interval: 50
                repeat: false
                running: false

                onTriggered: scrollView.positionViewAtEnd()
            }
        }

        MessageComposer {
            id: messageComposer

            Layout.fillWidth: true
        }
    }

    ConversationDetailsSheet {
        id: conversationDetailsSheet
        conversation: root.conversation
    }

    ConversationSettingsSheet {
        id: conversationSettingsSheet
        controller: root.controller
        conversation: root.conversation
    }

    EmojiDrawer {
        id: emojiDrawer
        controller: root.controller

        onEmojiSelected: console.log("Selected emoji " + name)
    }
}
