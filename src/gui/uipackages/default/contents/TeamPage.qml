// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

import QtQuick 2.7

import org.kde.kirigami 2.11 as Kirigami

Kirigami.Page {
    id: root

    property var team
    property var controller

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    TeamPageSideBar {
        id: sidebar

        team: root.team
        controller: root.controller

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
        width: parent.width * 0.3


        onConversationSelected: conversationStack.activeConversation = conversation
    }

    ConversationStack {
        id: conversationStack

        team: root.team
        controller: root.controller

        anchors {
            left: sidebar.right
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
    }

    Component.onCompleted: {
        console.log("Loaded TeamPage for '" + team.name + "'");
    }
}
