// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2

import org.kde.kirigami 2.11 as Kirigami

QQC2.TextArea {
    id: composer
}
