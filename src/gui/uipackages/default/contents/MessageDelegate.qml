// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0

MouseArea {
    id: root

    property var controller
    property var message

    signal emojiRequested()

    hoverEnabled: true
    height: childrenRect.height
    width: parent.width

    Rectangle {
        color: Kirigami.Theme.backgroundColor
        Kirigami.Theme.inherit: false
        Kirigami.Theme.colorSet: root.containsMouse ? Kirigami.Theme.View: Kirigami.Theme.Button
        height: childrenRect.height
        width: parent.width

        RowLayout {
            spacing: Kirigami.Units.smallSpacing

            Image {
                id: avatar
                Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                Layout.margins: Kirigami.Units.smallSpacing
                Layout.alignment: Qt.AlignTop

                source: "image://iconprovider/" + root.controller.team.id + "/" + root.message.user.id
                asynchronous: true
                fillMode: Image.PreserveAspectFit
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.margins: Kirigami.Units.smallSpacing

                spacing: Kirigami.Units.smallSpacing

                RowLayout {
                    Layout.fillWidth: true

                    QQC2.Label {
                        id: userName

                        text: root.message.user.displayName
                        font.weight: Font.Bold
                    }

                    QQC2.Label {
                        id: timestamp

                        text: root.message.timestamp.toLocaleTimeString("hh:mm")
                    }
                }

                TextEdit {
                    Layout.preferredHeight: contentHeight
                    Layout.maximumHeight: contentHeight
                    Layout.preferredWidth: root.width - avatar.width - 4 * Kirigami.Units.smallSpacing

                    readOnly: true
                    wrapMode: Text.WordWrap
                    selectByMouse: true
                    text: root.message.text
                }

                MessageReactionsBar {
                    Layout.fillWidth: true

                    controller: root.controller
                    message: root.message

                    visible: root.message.reactions.length > 0

                    onEmojiRequested: root.emojiRequested()
                }

                RowLayout {
                    visible: root.message.replyCount > 0
                    spacing: Kirigami.Units.largeSpacing

                    QQC2.Label {
                        text: i18np("1 reply", "%1 replies", root.message.replyCount)
                    }
                    QQC2.Label {
                        text: i18n("Last reply on %1", root.message.latestReply.toLocaleString(Locale.LongFormat))
                        color: Kirigami.Theme.disabledTextColor
                    }
                }
            }
        }

        RowLayout {
            anchors.right: parent.right
            anchors.top: parent.top
            visible: root.containsMouse

            QQC2.ToolButton {
                icon.name: "smiley-add"
                text: i18n("Add Reaction")
                display: QQC2.AbstractButton.IconOnly
                flat: true

                onClicked: root.emojiRequested()
            }
        }
    }
}
