// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.11

import org.kde.kirigami 2.11 as Kirigami

StackLayout {
    id: root

    property var team
    property var controller

    property var activeConversation

    property var __conversations: []

    onActiveConversationChanged: {
        for (var i = 0; i < __conversations.length; i++) {
            if (__conversations[i].conversation.id == activeConversation.id) {
                currentIndex = i;
                return;
            }
        }

        var newConversation = conversationViewComponent.createObject(root,
                {'conversation': activeConversation, 'controller': controller});
        __conversations.push(newConversation);
        currentIndex = count - 1;
        // HACK: force layout of the new view
        newConversation.width = width;
        newConversation.height = height;
    }

    Component {
        id: conversationViewComponent

        ConversationView {}
    }
}
