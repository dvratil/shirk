// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

Kirigami.OverlaySheet {
    id: sheet

    property var conversation
    property var controller

    header: Kirigami.Heading {
        text: conversation.name
    }

    ColumnLayout {
        width: parent.width

        QQC2.Button {
            Layout.alignment: Qt.AlignCenter

            id: leaveButton
            text: i18n("Leave Channel")
            icon.name: "view-close"
            enabled: !conversation.isGeneral

            onClicked: {
                controller.leaveChannel(conversation);
                sheet.close();
            }
        }

        QQC2.Label {
            Layout.alignment: Qt.AlignCenter
            text: i18n("The workspace general channel cannot be left")
            visible: conversation.isGeneral
        }
    }
}
