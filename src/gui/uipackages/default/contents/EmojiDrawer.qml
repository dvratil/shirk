// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.14
import QtQuick.Controls 2.14 as QQC2
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0 as Shirk

Kirigami.OverlayDrawer {
    id: emojiDrawer

    property var controller

    readonly property var __emojiModel: controller.emojiModel

    signal emojiSelected(string name)

    onDrawerOpenChanged: {
        searchField.text = ""
    }

    handleVisible: false
    edge: Qt.RightEdge

    Timer {
        id: searchTimer
        interval: 100
        repeat: false
        running: false

        onTriggered: {
            filterModel.filter = searchField.text
        }
    }

    contentItem: ColumnLayout {
        anchors.fill: parent

        Kirigami.SearchField {
            id: searchField

            Layout.fillWidth: true

            onTextChanged: {
                searchTimer.start();
            }
        }

        Kirigami.AbstractCard {
            id: searchCard
            visible: filterModel.filter !== ""

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignTop

            header: Kirigami.Heading {
                text: i18n("Search Results")
            }

            contentItem: EmojiGrid {
                id: searchGrid

                Layout.fillWidth: true
                Layout.fillHeight: true

                controller: emojiDrawer.controller
                interactive: true
                implicitHeight: 0.0

                model: Shirk.EmojiFilterModel {
                    id: filterModel
                    sourceModel: emojiDrawer.__emojiModel
                }

                onEmojiSelected: {
                    emojiDrawer.emojiSelected(name);
                    emojiDrawer.close();
                }
            }
        }

        Flickable {
            id: flickable
            Layout.fillHeight: true
            Layout.fillWidth: true

            visible: filterModel.filter === ""

            contentHeight: contentItem.childrenRect.height

            clip: true

            Kirigami.CardsLayout {
                id: categoryView

                maximumColumns: 1
                maximumColumnWidth: contentWidth
                minimumColumnWidth: maximumColumnWidth

                width: maximumColumnWidth + 2 * Kirigami.Units.largeSpacing

                Repeater {
                    model: emojiDrawer.__emojiModel.categories

                    delegate: Kirigami.AbstractCard {
                        id: card
                        property var categoryName: modelData

                        header: Kirigami.Heading {
                            text: categoryName
                        }

                        contentItem: EmojiGrid {
                            id: emojiGrid

                            Layout.fillWidth: true

                            controller: emojiDrawer.controller
                            model: Shirk.EmojiCategoryFilterModel {
                                sourceModel: emojiDrawer.__emojiModel
                                category: card.categoryName
                            }

                            onEmojiSelected: {
                                emojiDrawer.emojiSelected(name);
                                emojiDrawer.close();
                            }
                        }
                    }
                }
            }
        }
    }
}
