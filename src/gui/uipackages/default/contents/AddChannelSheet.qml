// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.7 as QQC2
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.11 as Kirigami
import org.kde.shirk 1.0

Kirigami.OverlaySheet {
    id: sheet

    property var team
    property var controller


    showCloseButton: true


    header: Kirigami.Heading {
        text: i18n("Add Channel")
    }

    contentItem: ColumnLayout {
        //height: mainWindow.height - 2 * Kirigami.Units.largeSpacing

        Kirigami.SearchField {
            id: searchField

            Layout.fillWidth: true
        }

        ListView {
            id: channelList

            Layout.preferredHeight: contentHeight
            Layout.fillWidth: true


            model: ConversationsFilterModel {
                sourceModel: controller.conversationsModel
                membershipFilter: ConversationsFilterModel.Membership.NotMember
                nameFilter: searchField.text
                typeFilter: ConversationsFilterModel.Type.Channel
            }

            delegate: Kirigami.AbstractListItem {
                id: delegate
                width: parent.width
                RowLayout {
                    ColumnLayout {
                        Layout.fillWidth: true

                        QQC2.Label {
                            text: '#' + model.conversation.name
                        }

                        QQC2.Label {
                            Layout.maximumWidth: delegate.width - joinButton.width - 4 * parent.spacing
                            readonly property var __purpose: model.conversation.purpose
                            visible: __purpose != null
                            text: __purpose ? __purpose : null
                            elide: Text.ElideRight
                        }
                    }

                    QQC2.Button {
                        id: joinButton
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        icon.name: "irc-join-channel"
                        text: i18n('Join')

                        onClicked: {
                            controller.joinChannel(model.conversation);
                            sheet.close();
                        }
                    }
                }
            }
        }
    }
}
