// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0

ColumnLayout {

    id: root

    property var conversation

    signal channelDetailsRequested()
    signal channelSettingsRequested()

    Kirigami.Heading {
        text: conversation.name
    }

    RowLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true

        QQC2.Label {
            id: topicLabel

            Layout.fillWidth: true

            text: conversation.topic
            elide: Text.ElideRight
        }

        QQC2.ToolButton {
            text: i18n("View Channel Details")
            icon.name: "documentinfo"
            display: QQC2.AbstractButton.IconOnly
            flat: true

            onClicked: root.channelDetailsRequested()
        }
        QQC2.ToolButton {
            text: i18n("Channel Settings")
            icon.name: "configure"
            display: QQC2.AbstractButton.IconOnly
            flat: true

            onClicked: root.channelSettingsRequested()
        }
    }
}
