// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0

Kirigami.ApplicationWindow {
    id: mainWindow

    title: i18n("Shirk")

    pageStack.separatorVisible: false
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None
    pageStack.interactive: false

    globalDrawer: Kirigami.GlobalDrawer {
        id: drawer

        collapsed: true
        showTopContentWhenCollapsed: true
        modal: false
        interactive: false

        topContent: Repeater {
            model: ShirkApplication.teamsModel

            Layout.fillWidth: true

            delegate: Kirigami.BasicListItem {
                property var team: model.team
                property var controller: model.controller

                icon: team.icon
                iconSize: Kirigami.Units.iconSizes.medium
                label: drawer.collapsed ? "" : team.name
                reserveSpaceForLabel: !drawer.collapsed

                padding: 0

                onClicked: pageStack.push(teamPageComponent, { "team": team, "controller": controller })
            }

            onItemAdded: {
                if (mainWindow.pageStack.currentItem !== null && mainWindow.pageStack.currentItem instanceof AddTeamPage) {
                    mainWindow.pageStack.pop();
                }
                if (mainWindow.pageStack.currentItem === null) {
                    mainWindow.pageStack.push(teamPageComponent, { "team": item.team, "controller": item.controller })
                }
            }
        }

        actions: [
            Kirigami.Action {
                text: i18n("Add Team")
                iconName: "list-add"

                onTriggered: {
                    if ((pageStack.currentItem === null) || (!(pageStack.currentItem instanceof AddTeamPage))) {
                        pageStack.push(addTeamPageComponent);
                    }
                }
            }
        ]
    }

    Component {
        id: addTeamPageComponent

        AddTeamPage {}
    }

    Component {
        id: teamPageComponent

        TeamPage {
        }
    }
}
