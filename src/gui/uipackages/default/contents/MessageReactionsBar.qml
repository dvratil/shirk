// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0

QQC2.ToolBar {
    id: root

    property var message
    property var controller

    signal emojiRequested()

    RowLayout {
        spacing: Kirigami.Units.smallSpacing

        Repeater {
            model: message.reactions

            delegate: QQC2.ToolButton {
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                // Workaround qqc2-desktop-style not supporting icon URLs other than file://
                Image {
                    anchors {
                        left: parent.left
                        top: parent.top
                        margins: 6
                    }
                    width: Kirigami.Units.iconSizes.smallMedium
                    height: Kirigami.Units.iconSizes.smallMedium
                    source: "image://emoji/" + root.controller.team.id + "/" + model.name
                }

                icon.name: "empty"
                icon.color: "transparent"

                text: model.count
                display: QQC2.AbstractButton.TextBesideIcon
            }
        }

        QQC2.ToolButton {
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

            icon.name: "smiley-add"
            text: i18n("Add Reaction")
            display: QQC2.AbstractButton.IconOnly

            onClicked: emojiRequested()
        }
    }
}
