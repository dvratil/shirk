// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

import QtQuick 2.7
import QtQuick.Controls 2.7 as QQC2
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.11 as Kirigami
import org.kde.shirk 1.0

Kirigami.AbstractListItem
{
    property var conversation

    RowLayout {
        width: parent.width

        QQC2.Label {
            text: '#'
        }

        QQC2.Label {
            Layout.fillWidth: true

            text: switch (conversation.type) {
                  case Conversation.Group:
                  case Conversation.Channel:
                    return conversation.name
                  default:
                    return conversation.user.name
                }

            font.weight: conversation.unreadCount > 0 ? Font.Bold : Font.Normal
        }

        QQC2.Label {
            visible: conversation.unreadCountDisplay > 0
            text: conversation.unreadCountDisplay
        }
    }

}

