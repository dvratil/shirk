// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC

import org.kde.kirigami 2.1 as Kirigami
import org.kde.shirk 1.0

Kirigami.Page {
    signal addTeamRequested()
    signal teamCreated()

    property var auth: ShirkApplication.startAddNewTeam()

    QQC.BusyIndicator {
        id: busyIndicator

        anchors.centerIn: parent

        running: true
    }

    QQC.Label {
        anchors {
            horizontalCenter: busyIndicator.horizontalCenter
            top: busyIndicator.bottom
        }

        horizontalAlignment: Qt.AlignCenter

        text: switch (auth.state) {
            case AuthController.WaitingForBrowser:
                return i18n("Please login into your Slack workspace in browser");
            case AuthController.RetrievingToken:
                return i18n("Retrieving information");
            case AuthController.Done:
                teamCreated();
                return i18n("Done");
            case AuthController.Error:
                return i18n("Error");
        }
    }

}
