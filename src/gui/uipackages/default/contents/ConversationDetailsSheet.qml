// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0

Kirigami.OverlaySheet {
    id: root

    property var conversation

    header: Kirigami.Heading {
        text: conversation.name
    }

    contentItem: Kirigami.CardsLayout {
        maximumColumns: 1
        width: parent.width

        Kirigami.Card {
            banner.title: i18n("Channel Details")
            contentItem: ColumnLayout {
                spacing: Kirigami.Units.largeSpacing

                QQC2.Label {
                    Layout.fillWidth: true
                    text: i18n("Created on %1 by %2", conversation.created.toLocaleString(Locale.LongFormat),
                                                      conversation.creator.displayName)
                    wrapMode: Text.WordWrap
                }
                QQC2.Label {
                    Layout.fillWidth: true
                    text: i18n("Member count: %1", conversation.memberCount)
                    wrapMode: Text.WordWrap
                }
                QQC2.Label {
                    visible: conversation.previousNames.length > 0
                    Layout.fillWidth: true
                    text: i18n("Previous names: %1", conversation.previousNames.join(", "))
                    wrapMode: Text.WordWrap
                }
            }
        }

        Kirigami.Card {
            visible: conversation.topic !== ""
            banner.title: i18n("Topic")
            contentItem: QQC2.Label {
                anchors.fill: parent
                text: conversation.topic
                wrapMode: Text.WordWrap
            }
            footer: QQC2.Label {
                text: i18n("Last set by %1 on %2", conversation.topicCreator.displayName,
                                                   conversation.topicLastSet.toLocaleString(Locale.LongFormat))
            }
        }

        Kirigami.Card {
            visible: conversation.purpose !== ""
            banner.title: i18n("Purpose")
            contentItem: QQC2.Label {
                anchors.fill: parent
                text: conversation.purpose
                wrapMode: Text.WordWrap
            }
            footer: QQC2.Label {
                text: i18n("Last set by %1 on %2", conversation.purposeCreator.displayName, conversation.purposeLastSet)
            }
        }
    }
}

