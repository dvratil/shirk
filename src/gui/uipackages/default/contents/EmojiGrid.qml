// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

import QtQuick 2.14
import QtQuick.Controls 2.14 as QQC2
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.11 as Kirigami

import org.kde.shirk 1.0 as Shirk

GridView {
    id: emojiView

    property var controller

    property int spacing: Kirigami.Units.smallSpacing

    property int iconSize: Kirigami.Units.iconSizes.smallMedium

    signal emojiSelected(string name)

    cellWidth: iconSize + 2 * Kirigami.Units.smallSpacing
    cellHeight: iconSize +  2 * Kirigami.Units.smallSpacing

    implicitHeight: contentHeight

    interactive: false
    flow: GridView.LeftToRight
    clip: true

    function tooltipText(name, aliases) {
        var str = ":" + name + ":";
        if (aliases !== undefined && aliases.length > 0) {
            for (var i = 0; i < aliases.length; ++i) {
                if (i != 0) {
                    str += ", ";
                }
                str += ":" + aliases[i] + ":";
            }
        }
        return str
    }

    delegate: MouseArea {
        id: mouseArea
        hoverEnabled: true
        focus: true

        width: emojiView.cellWidth
        height: emojiView.cellHeight

        Rectangle {
            color: mouseArea.containsMouse ? Kirigami.Theme.highlightColor : Kirigami.Theme.backgroundColor
            radius: 5

            anchors.fill: parent

            Image {
                width: emojiView.iconSize
                height: emojiView.iconSize
                anchors.centerIn: parent
                source: "image://emoji/" + (isCustomCategory ? controller.team.id : 'default') + "/" + name
                sourceSize.width: emojiView.emojiSize
                sourceSize.height: emojiView.emojiSize
                asynchronous: true
            }
        }

        QQC2.ToolTip.delay: 0
        QQC2.ToolTip.text: emojiView.tooltipText(name, aliases)
        QQC2.ToolTip.visible: containsMouse
        QQC2.ToolTip.toolTip.z: emojiDrawer.z + 1

        onClicked: emojiView.emojiSelected(name)
    }
}
