// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL


import QtQuick 2.7
import QtQuick.Controls 2.7 as QQC2
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.11 as Kirigami
import org.kde.shirk 1.0

Rectangle {
    id: root

    signal conversationSelected(var conversation)

    property var team
    property var controller

    Kirigami.Theme.colorSet: Kirigami.Theme.Complementary
    color: Kirigami.Theme.backgroundColor

    ColumnLayout {
        anchors {
            fill: parent
            margins: Kirigami.Units.largeSpacing * 2
        }


        QQC2.Label {
            text: team.name
            color: Kirigami.Theme.textColor
            font.pixelSize: Kirigami.Units.gridUnit * 1.3;
            font.weight: Font.Bold
            horizontalAlignment: Qt.AlignHCenter
        }

        ListView {
            id: conversationList

            Layout.fillHeight: true
            Layout.fillWidth: true

            model: ConversationsFilterModel {
                sourceModel :controller.conversationsModel
                membershipFilter: ConversationsFilterModel.Membership.Member
            }

            section {
                property: "group"
                criteria: ViewSection.FullString
                delegate: Kirigami.ListSectionHeader {
                    Kirigami.Theme.inherit: true
                    label: {
                        if (section === "Channels") {
                            return i18n("Channels")
                        } else if (section === "DirectChats") {
                            return i18n("Direct Messages");
                        } else if (section === "Starred") {
                            return i18n("Starred");
                        }
                    }

                    QQC2.ToolButton {
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        Kirigami.Theme.colorSet: Kirigami.Theme.Complementary

                        visible: section === "Channels"
                        icon.name: "list-add"
                        icon.color: Kirigami.Theme.textColor

                        onClicked: {
                            var sheet = addChannelSheetComponent.createObject(mainWindow)
                            sheet.open();
                            sheet.sheetOpenChanged.connect(function() {
                                sheet.destroy();
                            });
                        }
                    }

                }
                labelPositioning: ViewSection.InlineLabels
            }

            delegate: ConversationDelegate {
                conversation: model.conversation

                onClicked: root.conversationSelected(conversation)
            }
        }
    }

    Component {
        id: addChannelSheetComponent

        AddChannelSheet {
            team: root.team
            controller: root.controller
        }
    }
}
