// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <KAboutData>

namespace Shirk::Gui
{

class AboutData : public KAboutData
{
public:
    explicit AboutData();
};

}