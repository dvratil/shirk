// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>

#include "core/config.h"
#include "core/networkdispatcher.h"
#include "core/teamsmodel.h"

class QQmlEngine;

namespace Shirk::Core {
class AuthController;
}

namespace Shirk::Gui {

class QmlApplication : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Shirk::Core::TeamsModel* teamsModel READ teamsModel CONSTANT)
public:
    ~QmlApplication() override;

    static QmlApplication *instance(QQmlEngine &engine);
    static void destroy();

    static void registerQmlTypes();

    Core::TeamsModel *teamsModel();

    Q_INVOKABLE Shirk::Core::AuthController *startAddNewTeam();

private:
    explicit QmlApplication(QQmlEngine &engine);

private:
    QQmlEngine &mEngine;

    Core::Config mConfig;
    Core::NetworkDispatcher mNetworkDispatcher;
    Core::TeamsModel mTeamsModel;
};

}
