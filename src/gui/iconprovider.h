// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QQuickImageProvider>

namespace Shirk::Core
{
class TeamsModel;
}

namespace Shirk::Gui
{

class IconProvider : public QQuickImageProvider
{
public:
    explicit IconProvider(Core::TeamsModel &teams);

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) override;

private:
    Core::TeamsModel &mModel;
};

} // namespace
