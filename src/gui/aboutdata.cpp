// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "aboutdata.h"
#include "utils/stringliterals.h"
#include "shirk_version.h"

#include <KLocalizedString>
#include <QString>

using namespace Shirk::Gui;
using namespace Shirk::StringLiterals;

AboutData::AboutData()
    : KAboutData{u"shirk"_qs,
                 i18n("Shirk"),
                 QStringLiteral(SHIRK_VERSION_STRING),
                 i18n("An Unofficial Slack Client"),
                 KAboutLicense::GPL_V3,
                 {} /* copyright */,
                 {} /* other */,
                 u"https://invent.kde.org/dvratil/shirk"_qs} {
    addAuthor(i18n("Daniel Vrátil"), i18n("Maintainer"), i18n("dvratil@kde.org"));


    setApplicationData(*this);
}