// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "application.h"
#include "utils/stringliterals.h"
#include "gui_debug.h"
#include "qmlapplication.h"

#include "core/teamcontroller.h"
#include "core/teamsmodel.h"

#include <KPackage/PackageLoader>
#include <KPackage/Package>

#include <QFileInfo>
#include <QDir>
#include <QQmlContext>
#include <QWindow>

using namespace Shirk::Gui;
using namespace Shirk::StringLiterals;

Application::Application(int &argc, char **argv)
    : QApplication(argc, argv) {

    setupQmlEngine();
    setupUi();
}

Application::~Application() {
    QmlApplication::destroy();
}

void Application::setupQmlEngine() {
    QmlApplication::registerQmlTypes();

    qmlRegisterSingletonType<QmlApplication>("org.kde.shirk", 1, 0, "ShirkApplication",
                                             [](QQmlEngine *engine, QJSEngine *) -> QObject* {
                                                auto instance = QmlApplication::instance(*engine);
                                                engine->setObjectOwnership(instance, QQmlEngine::CppOwnership);
                                                return instance;
                                            });

    mKDeclarative.setDeclarativeEngine(&mEngine);
    mKDeclarative.setupContext();
    mKDeclarative.setupEngine(&mEngine);
}

void Application::setupUi() {
#ifndef QT_NDEBUG
    const auto mainFile = u"src/gui/uipackages/default/contents/main.qml"_qs;

    QFileInfo appFile{QCoreApplication::applicationFilePath()};
    auto dir = appFile.absoluteDir(); // should be /some/path/shirk/build/bin
    dir.cdUp();
    dir.cdUp(); // cd up to top-level
    if (QFileInfo mainFinfo{dir, mainFile}; mainFinfo.exists()) {
        qCInfo(LOG_GUI) << "Loading main QML file from " << mainFinfo.absoluteFilePath();
        mEngine.load(QUrl::fromLocalFile(mainFinfo.absoluteFilePath()));
        return;
    }
#endif

    loadPackageUi(u"default"_qs);
}

bool Application::loadPackageUi(const QString &packageName) {
    const auto prefix = u"org.kde.shirk.uipackages."_qs;
    const auto serviceType = u"Shirk/UiPackage"_qs;
    const auto packageLoader = KPackage::PackageLoader::self();

    const auto package = packageLoader->loadPackage(serviceType, prefix + packageName);
    if (!package.isValid()) {
        qCCritical(LOG_GUI) << "Error loading UI package: " << packageName << "is not a valid package";
        qCDebug(LOG_GUI) << "Available Qt Quick Ui packages for Shirk:";
        for (const auto pkg : packageLoader->listPackages(serviceType)) {
            qCDebug(LOG_GUI) << "\t" << pkg.name() << "/" << pkg.pluginId();
        }
        return false;
    }

    mEngine.load(QUrl::fromLocalFile(package.filePath("window")));

    return true;
}

void Application::raiseMainWindow() {
    qCDebug(LOG_GUI) << "Raising main window";
    const auto isApplicationWindow = [](QObject *obj) {
        if (const auto window = qobject_cast<QWindow *>(obj); window) {
            return window->isTopLevel();
        }
        return false;
    };

    const auto rootObjects = mEngine.rootObjects();
    const auto windowIt = std::find_if(rootObjects.cbegin(), rootObjects.cend(), isApplicationWindow);
    if (windowIt != rootObjects.cend()) {
        auto window = qobject_cast<QWindow *>(*windowIt);
        window->requestActivate();
        window->raise();
        window->showNormal();
    } else {
        qCWarning(LOG_GUI) << "Failed to find application window!";
    }
}

