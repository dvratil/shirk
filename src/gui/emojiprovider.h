// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QQuickAsyncImageProvider>
#include <QThreadStorage>

class QQuickImageResponse;

namespace Shirk::Core
{
class TeamsModel;
}

namespace Shirk::Gui
{

class EmojiProvider : public QQuickAsyncImageProvider
{
public:
    explicit EmojiProvider(Core::TeamsModel &teams);

    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override;

private:
    Core::TeamsModel &mModel;
    const QImage mSheet;
};

}
