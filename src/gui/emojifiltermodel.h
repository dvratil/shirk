// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QSortFilterProxyModel>

#include <memory>

class KDescendantsProxyModel;

namespace Shirk::Gui
{

class EmojiFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
public:
    explicit EmojiFilterModel(QObject *parent = {});
    ~EmojiFilterModel() override;

    void setSourceModel(QAbstractItemModel *sourceModel) override;

    void setFilter(const QString &filter);
    QString filter() const;

Q_SIGNALS:
    void filterChanged(const QString &filter);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:
    std::unique_ptr<KDescendantsProxyModel> mFlattenModel;
    QString mFilter;
};

}
