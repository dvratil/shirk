// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "core/conversation.h"

#include <QSortFilterProxyModel>
#include <QString>

namespace Shirk::Gui {

class ConversationsFilterModel: public QSortFilterProxyModel {
    Q_OBJECT


    Q_PROPERTY(MembershipFlag membershipFilter READ membershipFilter WRITE setMembershipFilter NOTIFY membershipFilterChanged)
    Q_PROPERTY(QString nameFilter READ nameFilter WRITE setNameFilter NOTIFY nameFilterChanged)
    Q_PROPERTY(Types typeFilter READ typeFilter WRITE setTypeFilter NOTIFY typeFilterChanged)

public:
    enum class Type {
        Channel = 0x01,
        Group = 0x02,
        IM = 0x04,
        MPIM = 0x08,

        AllTypes = Channel | Group | IM | MPIM
    };
    Q_ENUM(Type)
    Q_DECLARE_FLAGS(Types, Type)
    Q_FLAG(Types)

    enum class Membership {
        Member,
        NotMember,

        All = Member | NotMember
    };
    Q_ENUM(Membership)
    Q_DECLARE_FLAGS(MembershipFlag, Membership)
    Q_FLAG(MembershipFlag)

    explicit ConversationsFilterModel(QObject *parent = nullptr);

    MembershipFlag membershipFilter() const;
    void setMembershipFilter(MembershipFlag membership);

    QString nameFilter() const;
    void setNameFilter(const QString &nameFilter);

    Types typeFilter() const;
    void setTypeFilter(Types type);

Q_SIGNALS:
    void membershipFilterChanged();
    void nameFilterChanged();
    void typeFilterChanged();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override;

private:
    MembershipFlag mMembership = Membership::All;
    QString mNameFilter;
    Types mTypes = Type::AllTypes;
};

}

Q_DECLARE_OPERATORS_FOR_FLAGS(Shirk::Gui::ConversationsFilterModel::Types)
