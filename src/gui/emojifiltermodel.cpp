// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "emojifiltermodel.h"
#include "core/emojimodel.h"

#include <qnamespace.h>
#include <range/v3/algorithm/any_of.hpp>

#include <KDescendantsProxyModel>

using namespace Shirk::Gui;

EmojiFilterModel::EmojiFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{}

EmojiFilterModel::~EmojiFilterModel() = default;

void EmojiFilterModel::setSourceModel(QAbstractItemModel *sourceModel)
{
    mFlattenModel = std::make_unique<KDescendantsProxyModel>();
    mFlattenModel->setSourceModel(sourceModel);
    QSortFilterProxyModel::setSourceModel(mFlattenModel.get());
}

void EmojiFilterModel::setFilter(const QString &filter)
{
    if (mFilter != filter) {
        mFilter = filter;
        Q_EMIT filterChanged(mFilter);
        invalidateFilter();
    }
}

QString EmojiFilterModel::filter() const
{
    return mFilter;
}

bool EmojiFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    const auto source_index = sourceModel()->index(source_row, 0, source_parent);
    const auto name = sourceModel()->data(source_index, Core::EmojiModel::NameRole).toString();

    if (name.contains(mFilter, Qt::CaseInsensitive)) {
        return true;
    }

    const auto aliases = sourceModel()->data(source_index, Core::EmojiModel::AliasesRole).toStringList();
    return ranges::any_of(aliases, [this](const QString &alias) { return alias.contains(mFilter); });
}


