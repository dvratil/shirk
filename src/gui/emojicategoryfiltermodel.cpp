// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "emojicategoryfiltermodel.h"
#include "core/emojimodel.h"

#include <range/v3/algorithm/find_if.hpp>
#include <range/v3/algorithm/any_of.hpp>

using namespace Shirk::Gui;

EmojiCategoryFilterModel::EmojiCategoryFilterModel(QObject *parent)
    : QIdentityProxyModel(parent)
{}

void EmojiCategoryFilterModel::setSourceModel(QAbstractItemModel *sourceModel)
{
    mCategoryIndex = QModelIndex{} ;
    QIdentityProxyModel::setSourceModel(sourceModel);

    updateCategoryIndex();
}

void EmojiCategoryFilterModel::updateCategoryIndex()
{
    mCategoryIndex = QModelIndex{};
    if (mCategory.isEmpty() || sourceModel() == nullptr) {
        return;
    }

    // TODO: model iterator?
    for (int i = 0; i < sourceModel()->rowCount(); ++i) {
        const auto index = sourceModel()->index(i, 0);
        if (index.data() == mCategory) {
            mCategoryIndex = index;
            return;
        }
    }
}

QString EmojiCategoryFilterModel::category() const
{
    return mCategory;
}

void EmojiCategoryFilterModel::setCategory(const QString &category)
{
    if (mCategory == category) {
        if ((mCategory.isEmpty() && !mCategoryIndex.isValid())
                || !(mCategory.isEmpty() && mCategoryIndex.isValid())) {
            return;
        }
    }

    beginResetModel();
    mCategory = category;
    Q_EMIT categoryChanged();
    updateCategoryIndex();
    endResetModel();
}

int EmojiCategoryFilterModel::rowCount(const QModelIndex &parent) const
{
    if (sourceModel() == nullptr) {
        return 0;
    }

    if (mCategoryIndex.isValid()) {
        return parent.isValid() ? 0 : sourceModel()->rowCount(mCategoryIndex);
    }

    return QIdentityProxyModel::rowCount(parent);
}

int EmojiCategoryFilterModel::columnCount(const QModelIndex &) const
{
    return sourceModel() ? sourceModel()->columnCount() : 0;
}

QModelIndex EmojiCategoryFilterModel::index(int row, int column, const QModelIndex &parent) const
{
    if (sourceModel() == nullptr) {
        return {};
    }

    if (mCategoryIndex.isValid()) {
        return parent.isValid() ? QModelIndex{} : createIndex(row, column);
    }

    return QIdentityProxyModel::index(row, column, parent);
}

QModelIndex EmojiCategoryFilterModel::parent(const QModelIndex &child) const
{
    if (sourceModel() == nullptr) {
        return {};
    }

    if (mCategoryIndex.isValid()) {
        return {};
    }

    return QIdentityProxyModel::parent(child);
}

QModelIndex EmojiCategoryFilterModel::mapToSource(const QModelIndex &proxyIndex) const
{
    if (sourceModel() == nullptr) {
        return {};
    }

    if (mCategoryIndex.isValid()) {
        return sourceModel()->index(proxyIndex.row(), proxyIndex.column(), mCategoryIndex);
    }

    return QIdentityProxyModel::mapToSource(proxyIndex);
}

QModelIndex EmojiCategoryFilterModel::mapFromSource(const QModelIndex &sourceIndex) const
{
    if (sourceModel() == nullptr) {
        return {};
    }

    if (mCategoryIndex.isValid()) {
        if (sourceIndex.parent() == mCategoryIndex) {
            return createIndex(sourceIndex.row(), sourceIndex.column());
        } else {
            return {};
        }
    }

    return QIdentityProxyModel::mapFromSource(sourceIndex);
}

