// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "qmlapplication.h"
#include "conversationsfiltermodel.h"
#include "iconprovider.h"
#include "emojiprovider.h"
#include "emojicategoryfiltermodel.h"
#include "emojifiltermodel.h"
#include "core/authcontroller.h"
#include "core/team.h"
#include "core/conversation.h"
#include "core/teamcontroller.h"
#include "core/conversationsmodel.h"
#include "core/messagemodel.h"
#include "utils/stringliterals.h"
#include "utils/memory.h"
#include "gui_debug.h"

#include <QQmlEngine>

using namespace Shirk;
using namespace Shirk::Gui;
using namespace Shirk::StringLiterals;

Q_DECLARE_METATYPE(Shirk::Core::TeamsModel*)
Q_DECLARE_METATYPE(Shirk::Core::AuthController*)
Q_DECLARE_METATYPE(Shirk::Core::Conversation*)
Q_DECLARE_METATYPE(Shirk::Core::User*)

static UniqueQObjectPtr<QmlApplication> s_instance = nullptr;

QmlApplication::QmlApplication(QQmlEngine &engine)
    : QObject()
    , mEngine(engine)
{
    qCDebug(LOG_GUI) << "Created QmlApplication";
    mTeamsModel.loadControllers(mConfig);

    mEngine.addImageProvider(u"iconprovider"_qs, new IconProvider{mTeamsModel});
    mEngine.addImageProvider(u"emoji"_qs, new EmojiProvider{mTeamsModel});
}

QmlApplication::~QmlApplication() {}

void QmlApplication::registerQmlTypes() {
    qRegisterMetaType<Core::User*>();

    qmlRegisterUncreatableType<Core::TeamsModel>("org.kde.shirk", 1, 0, "TeamsModel", u"Use ShirkApplication instead"_qs);
    qmlRegisterUncreatableType<Core::Team>("org.kde.shirk", 1, 0, "Team", u"Use TeamController instead"_qs);
    qmlRegisterUncreatableType<Core::AuthController>("org.kde.shirk", 1, 0, "AuthController", u"Use ShirkApplication instead"_qs);
    qmlRegisterUncreatableType<Core::Conversation>("org.kde.shirk", 1, 0, "Conversation", u"Use ConversationModel instead"_qs);
    qmlRegisterUncreatableType<Core::ConversationsModel>("org.kde.shirk", 1, 0, "ConversationsModel", u"Use ShirkApplication instead"_qs);
    qmlRegisterUncreatableType<Core::User>("org.kde.shirk", 1, 0, "User", u"User cannot be instantiated in QML"_qs);
    qmlRegisterUncreatableType<Core::MessageModel>("org.kde.shirk", 1, 0, "MessageModel", u"Use TeamController instead"_qs);
    qmlRegisterUncreatableType<Core::Reaction>("org.kde.shirk", 1, 0, "Reaction", u"Use MessageModel instead"_qs);

    qmlRegisterType<ConversationsFilterModel>("org.kde.shirk", 1, 0, "ConversationsFilterModel");
    qmlRegisterType<EmojiCategoryFilterModel>("org.kde.shirk", 1, 0, "EmojiCategoryFilterModel");
    qmlRegisterType<EmojiFilterModel>("org.kde.shirk", 1, 0, "EmojiFilterModel");
}

QmlApplication *QmlApplication::instance(QQmlEngine &engine) {
    if (!s_instance) {
        s_instance  = UniqueQObjectPtr<QmlApplication>(new QmlApplication(engine));
    }

    return s_instance.get();
}

void QmlApplication::destroy() {
    s_instance.reset();
}

Core::TeamsModel *QmlApplication::teamsModel() {
    return &mTeamsModel;
}

Core::AuthController *QmlApplication::startAddNewTeam()
{
    auto controller = new Core::AuthController();
    mEngine.setObjectOwnership(controller, QQmlEngine::JavaScriptOwnership);
    controller->start().then([this](std::unique_ptr<Core::Team> team) {
        auto teamController = std::make_unique<Core::TeamController>(std::move(team));
        mTeamsModel.addTeamController(std::move(teamController), mConfig);
    });
    return controller;
}

