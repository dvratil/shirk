// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "conversationsfiltermodel.h"
#include "conversation.h"
#include "core/conversationsmodel.h"

using namespace Shirk;
using namespace Shirk::Gui;

ConversationsFilterModel::ConversationsFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    setDynamicSortFilter(true);
}

ConversationsFilterModel::MembershipFlag ConversationsFilterModel::membershipFilter() const
{
    return mMembership;
}

void ConversationsFilterModel::setMembershipFilter(MembershipFlag membership)
{
    if (mMembership != membership) {
        mMembership = membership;
        Q_EMIT membershipFilterChanged();
        invalidateFilter();
    }
}

QString ConversationsFilterModel::nameFilter() const
{
    return mNameFilter;
}

void ConversationsFilterModel::setNameFilter(const QString &nameFilter)
{
    if (mNameFilter != nameFilter) {
        mNameFilter = nameFilter;
        Q_EMIT nameFilterChanged();
        invalidateFilter();
    }
}

ConversationsFilterModel::Types ConversationsFilterModel::typeFilter() const
{
    return mTypes;
}

void ConversationsFilterModel::setTypeFilter(Types types)
{
    if (mTypes != types) {
        mTypes = types;
        Q_EMIT typeFilterChanged();
        invalidateFilter();
    }
}

namespace {

ConversationsFilterModel::Type flagForConversationType(Core::Conversation::Type type)
{
    switch (type) {
    case Core::Conversation::Type::Channel:
        return ConversationsFilterModel::Type::Channel;
    case Core::Conversation::Type::Group:
        return ConversationsFilterModel::Type::Group;
    case Core::Conversation::Type::IM:
        return ConversationsFilterModel::Type::IM;
    case Core::Conversation::Type::MPIM:
        return ConversationsFilterModel::Type::MPIM;
    }

    Q_UNREACHABLE();
}

}

bool ConversationsFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    const auto *conversation = sourceModel()->data(sourceModel()->index(sourceRow, 0, sourceParent),
                                                   Core::ConversationsModel::ConversationRole).value<Core::Conversation*>();

    if ((conversation->isMember() && !mMembership.testFlag(Membership::Member))
        || (!conversation->isMember() && !mMembership.testFlag(Membership::NotMember))) {
        return false;
    }

    if (!(mTypes & flagForConversationType(conversation->type()))) {
        return false;
    }

    if (!mNameFilter.isEmpty()) {
        return conversation->name().contains(mNameFilter);
    }

    return true;
}

bool ConversationsFilterModel::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const
{
    const auto groupLeft = sourceModel()->data(sourceLeft, Core::ConversationsModel::GroupRole).value<Core::ConversationsModel::Group>();
    const auto groupRight = sourceModel()->data(sourceRight, Core::ConversationsModel::GroupRole).value<Core::ConversationsModel::Group>();

    if (groupLeft == groupRight) {
        const auto convLeft = sourceModel()->data(sourceLeft, Qt::DisplayRole).toString();
        const auto convRight = sourceModel()->data(sourceRight, Qt::DisplayRole).toString();
        return QString::localeAwareCompare(convLeft, convRight) < 0;
    }

    if (groupLeft == Core::ConversationsModel::Group::Starred) {
        return true;
    } else if (groupLeft == Core::ConversationsModel::Group::Channels && groupRight == Core::ConversationsModel::Group::DirectChats) {
        return true;
    }

    return false;
}
