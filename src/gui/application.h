// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "aboutdata.h"

#include <QApplication>
#include <QQmlApplicationEngine>

#include <KDeclarative/KDeclarative>

#include <memory>

namespace Shirk::Gui
{

class QmlApplication;
class Application : public QApplication
{
    Q_OBJECT

public:
    explicit Application(int &argc, char **argv);
    ~Application() override;

    void raiseMainWindow();

private:
    void setupQmlEngine();
    void setupUi();
    bool loadPackageUi(const QString &packageName);

private:
    std::unique_ptr<QmlApplication> mApplication;
    AboutData mAboutData;
    QQmlApplicationEngine mEngine;
    KDeclarative::KDeclarative mKDeclarative;
};

}
