// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QIdentityProxyModel>
#include <QPersistentModelIndex>

namespace Shirk::Gui
{

class EmojiCategoryFilterModel : public QIdentityProxyModel
{
    Q_OBJECT

    Q_PROPERTY(QString category READ category WRITE setCategory NOTIFY categoryChanged)
public:

    explicit EmojiCategoryFilterModel(QObject *parent = nullptr);
    ~EmojiCategoryFilterModel() = default;

    void setSourceModel(QAbstractItemModel *source) override;

    QString category() const;
    void setCategory(const QString &category);

    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;

    QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
    QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;

Q_SIGNALS:
    void categoryChanged();

private:
    void updateCategoryIndex();

    QString mCategory;
    QPersistentModelIndex mCategoryIndex;
};


} // namespace
