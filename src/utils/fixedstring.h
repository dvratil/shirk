// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <cstddef>

#include <QStringView>

namespace Shirk {

template<typename CharT, std::size_t Size>
struct BasicFixedString {
    constexpr BasicFixedString(const CharT (&in)[Size + 1]) {
        copy_n(in, Size + 1, m_data);
    }

    constexpr const CharT* str() const { return m_data; }
private:
    // FIXME: std::copy_n is not constexpr in gcc 9
    constexpr void copy_n(const CharT *input, std::size_t len, CharT *result) {
        if (len > 0) {
            while (true) {
                *result = *input;
                ++result;
                if (--len > 0) {
                    ++input;
                } else {
                    break;
                }
            }
        }
    }

    CharT m_data[Size + 1]{};
};

template<typename CharT, std::size_t Size>
BasicFixedString(const CharT (&str)[Size]) -> BasicFixedString<CharT, Size - 1>;

template<std::size_t Size>
using FixedString = BasicFixedString<char, Size>;

template<std::size_t Size>
struct FixedUnicodeString : BasicFixedString<char16_t, Size> {
    constexpr FixedUnicodeString(const char16_t (&in)[Size + 1])
        : BasicFixedString<char16_t, Size>(in) {}

    constexpr operator QStringView() const { return QStringView{this->m_data, Size}; }
};

template<std::size_t Size>
FixedUnicodeString(const char16_t (&str)[Size]) -> FixedUnicodeString<Size - 1>;

} // namespace
