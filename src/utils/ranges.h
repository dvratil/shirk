// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <iterator>
#include <range/v3/range/conversion.hpp>

#include <QList>
#include <QVector>
#include <type_traits>

namespace ranges {

    namespace detail {

        // FIXME: This shouldn't be needed since Qt 5.14
        template<typename ContT>
        struct qt_container_wrapper : public ContT
        {
        public:
            using container_type = ContT;

            template<typename It>
            explicit qt_container_wrapper(It &&first, It &&last)
                : ContT()
            {
                static_assert(sizeof(qt_container_wrapper<ContT>) == sizeof(ContT));
                this->reserve(static_cast<typename ContT::size_type>(std::distance(first, last)));
                if constexpr (std::is_copy_constructible_v<typename ContT::value_type>) {
                    std::copy(first, last, std::back_inserter(*this));
                } else {
                    std::move(first, last, std::back_inserter(*this));
                }
            }
        };

        struct to_qtcontainer {
            template<typename MetaFn>
            struct fn;

            template<typename MetaFn, typename Fn>
            struct closure;

            template<typename MetaFn, typename Rng>
            using container_t = meta::invoke<MetaFn, Rng>;
        };

        struct RANGES_STRUCT_WITH_ADL_BARRIER(to_qtcontainer_closure_base)
        {
            template<typename Rng, typename MetaFn, typename Fn>
            friend constexpr auto operator|(Rng &&rng, to_qtcontainer::closure<MetaFn, Fn> fn) {
                return static_cast<Fn &&>(fn)(static_cast<Rng &&>(rng));
            }
        };

        template<typename MetaFn, typename Fn>
        struct to_qtcontainer::closure : to_qtcontainer_closure_base, Fn
        {
            closure() = default;
            constexpr explicit closure(Fn fn)
                : Fn(static_cast<Fn &&>(fn))
            {}

            template<typename Rng>
            friend constexpr auto operator|(Rng &&rng, to_qtcontainer::closure<MetaFn, Fn> fn)
            {
                return static_cast<Fn &&>(fn)(static_cast<Rng &&>(rng));
            }
        };

        template<typename MetaFn>
        struct to_qtcontainer::fn
        {
        public:
            template<typename Rng> requires input_range<Rng>
            auto operator()(Rng &&rng) const -> typename container_t<MetaFn, Rng>::container_type
            {
                using cont_t = container_t<MetaFn, Rng>;
                return cont_t{ranges::begin(rng), ranges::end(rng)};
            }
        };

        template<typename MetaFn, typename Fn>
        using to_qtcontainer_closure = to_qtcontainer::closure<MetaFn, Fn>;

        template<typename MetaFn>
        using to_qtcontainer_fn = to_qtcontainer_closure<MetaFn, to_qtcontainer::fn<MetaFn>>;

        template<typename ContT>
        struct from_range_qt
        {
            template<typename Rng>
            static auto from_rng_(int) -> decltype(ContT(range_cpp17_iterator_t<Rng>{},
                                                         range_cpp17_iterator_t<Rng>{}));
            template<typename Rng>
            static auto from_rng_(long) -> ContT;

            template<typename Rng>
            using invoke = decltype(from_range_qt::from_rng_<Rng>(0));
        };
    } // detail

    template<typename ContT>
    auto to_qt(detail::to_qtcontainer = {}) -> detail::to_qtcontainer_fn<detail::from_range_qt<detail::qt_container_wrapper<ContT>>>
    {
        return {};
    }

    template<typename ContT, typename Rng>
    auto to_qt(Rng &&rng)
    {
        return detail::to_qtcontainer_fn<detail::from_range_qt<detail::qt_container_wrapper<ContT>>>{}(static_cast<Rng &&>(rng));
    }
}
