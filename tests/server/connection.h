// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QThread>
#include <QHostAddress>

class QTcpSocket;

enum class Method {
    GET,
    POST
};

class Connection : public QThread
{
    Q_OBJECT
public:
    explicit Connection(std::unique_ptr<QTcpSocket> socket);
    ~Connection() override;

    void run() override;

Q_SIGNALS:
    void closed();

private:
    using Headers = QMap<QByteArray, QByteArray>;

    void handleRequest();
    std::pair<Method, QUrl> parseInitialPath(const QByteArray &path);
    Headers parseHeaders();
    QByteArray parseBody(const Headers &headers);
    void writeResponse(const QByteArray &response);

    std::ostream &log();
private:
    std::unique_ptr<QTcpSocket> mSocket;
    std::pair<QHostAddress, uint16_t> mPeer;
};
