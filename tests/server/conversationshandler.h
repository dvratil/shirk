// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "connection.h"

class QUrlQuery;
class QByteArray;

namespace Handler::Conversations
{

QByteArray list(Method method, const QUrlQuery &query, const QByteArray &data);

}
