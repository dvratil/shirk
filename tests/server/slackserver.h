// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QTcpServer>
#include <QTcpSocket>

#include "utils/memory.h"

class Connection;

class SlackServer : public QObject {
    Q_OBJECT
public:
    explicit SlackServer();
    ~SlackServer() override;


private:
    QTcpServer mServer;
    std::vector<Shirk::UniqueQObjectPtr<Connection>> mConnections;
};
