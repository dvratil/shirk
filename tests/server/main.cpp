// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include <QCoreApplication>

#include "slackserver.h"

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    SlackServer server;

    return app.exec();
}
