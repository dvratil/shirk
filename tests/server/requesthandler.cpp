// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "requesthandler.h"
#include "conversationshandler.h"

#include <QUrl>
#include <QUrlQuery>
#include <QByteArrayMatcher>

#include <array>
#include <stdexcept>
#include <tuple>

using namespace std::string_literals;

namespace impl {

using HandlerFunc = QByteArray(*)(Method, const QUrlQuery &, const QByteArray &);

template<uint N>
struct Handler {
    QStaticByteArrayMatcher<N> pattern;
    HandlerFunc handler;
};

template<uint N>
constexpr auto make_request(const char (&pattern)[N], HandlerFunc func)
{
    return Handler<N>{QStaticByteArrayMatcher<N>{pattern}, func};
}

static constexpr auto handlers = std::make_tuple(
        make_request("/api/conversations.list", ::Handler::Conversations::list)
);

struct Request {
    Method method;
    const QByteArray &path;
    const QUrlQuery &query;
    const QByteArray &data;
};

struct Match {

template<typename Handler, typename ... Tail>
QByteArray operator()(Request &&request, const Handler &handler, const Tail & ...tail)
{
    if (handler.pattern.indexIn(request.path) > -1) {
        return handler.handler(request.method, request.query, request.data);
    } else {
        if constexpr (sizeof...(Tail) > 0) {
            return (*this)(request, std::forward<Tail>(tail) ...);
        } else {
            throw std::runtime_error("Unknown path "s + request.path.data());
        }
    }
}

};

} // namespace

static QByteArray match(Method method, const QByteArray &path, const QUrlQuery &query, const QByteArray &data)
{
    return std::apply(impl::Match{}, std::tuple_cat(std::make_tuple(impl::Request{method, path, query, data}), impl::handlers));
}

QByteArray RequestHandler::handleRequest(Method method, const QUrl &url, const QByteArray &data)
{
    const auto path = url.path().toUtf8();
    return match(method, path, QUrlQuery(url), data);
}
