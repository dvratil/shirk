// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include "connection.h"

namespace RequestHandler
{

QByteArray handleRequest(Method method, const QUrl &path, const QByteArray &body);

}
