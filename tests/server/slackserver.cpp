// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "slackserver.h"
#include "connection.h"

#include <QThread>
#include <QDebug>

SlackServer::SlackServer() {
    if (!mServer.listen(QHostAddress::Any, 54321)) {
        qCritical("Failed to start server: %s", qUtf8Printable(mServer.errorString()));
        return;
    }

    connect(&mServer, &QTcpServer::newConnection,
            this, [this]() {
                std::unique_ptr<QTcpSocket> socket{mServer.nextPendingConnection()};
                socket->setParent(nullptr);
                auto connection = Shirk::make_unique_qobject<Connection>(std::move(socket));
                connection->start();
                connect(connection.get(), &Connection::closed,
                        this, [this, conn = connection.get()]() {
                            std::erase_if(mConnections, Shirk::UniquePtrCompare{conn});
                        });

                mConnections.emplace_back(std::move(connection));
            });
}

SlackServer::~SlackServer() = default;
