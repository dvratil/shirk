// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "conversationshandler.h"

QByteArray Handler::Conversations::list(Method, const QUrlQuery &, const QByteArray &)
{
    return {};   
}
