// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "connection.h"
#include "requesthandler.h"

#include <QTcpSocket>
#include <QDebug>
#include <QDateTime>
#include <QHostAddress>
#include <QDataStream>
#include <QUrl>
#include <QUrlQuery>

#include <iostream>
#include <limits>

using namespace std::string_literals;

std::ostream &operator<<(std::ostream &str, Method method)
{
    switch (method) {
    case Method::GET:
        return str << "GET";
    case Method::POST:
        return str << "POST";
    }

    Q_UNREACHABLE();
}

Connection::Connection(std::unique_ptr<QTcpSocket> socket)
    : mSocket(std::move(socket))
{
    mSocket->moveToThread(this);
    mPeer = std::make_pair(mSocket->peerAddress(), mSocket->peerPort());
}

Connection::~Connection() = default;

std::ostream &Connection::log()
{
    std::cout << "[" << qUtf8Printable(QDateTime::currentDateTime().toString()) << "] "
              << qUtf8Printable(mPeer.first.toString()) << ":" << mPeer.second
              << " - ";
    return std::cout;
}

void Connection::run()
{
    log() << "Connection started" << std::endl;
    while (true) {
        mSocket->waitForReadyRead();
        if (mSocket->state() == QAbstractSocket::UnconnectedState) {
            break;
        }

        try {
            handleRequest();
        } catch (const std::runtime_error &e) {
            qCritical("%s", e.what());
            mSocket->close();
            break;
        }
    }
    log() << "Connection terminated" << std::endl;
}

void Connection::handleRequest()
{
    const auto [method, path] = parseInitialPath(mSocket->readLine());
    const auto headers = parseHeaders();
    const auto body = parseBody(headers);
    log() << method << " " << qUtf8Printable(path.toString()) << " " << body.data() << std::endl;

    const auto response = RequestHandler::handleRequest(method, path, body);

    writeResponse(response);
}

std::pair<Method, QUrl> Connection::parseInitialPath(const QByteArray &ba)
{
    const auto verbEnd = ba.indexOf(' ');
    const auto verb = ba.left(verbEnd);

    Method method;
    if (verb == "GET") {
        method = Method::GET;
    } else if (verb == "POST") {
        method = Method::POST;
    } else {
        throw std::runtime_error("Unsupported method "s + verb.data());
    }

    const auto pathEnd = ba.indexOf(' ', verbEnd + 1);
    const auto path = ba.mid(verbEnd + 1, pathEnd - verbEnd - 1);

    QUrl url;
    const auto queryStart = path.indexOf('?');
    if (queryStart > -1) {
        url.setPath(QString::fromUtf8(path.left(queryStart)));
        url.setQuery(QString::fromUtf8(path.mid(queryStart + 1)));
    } else {
        url.setPath(QString::fromUtf8(path));
    }

    return {method, url};
}

Connection::Headers Connection::parseHeaders()
{
    Headers results;
    auto line = mSocket->readLine().trimmed();
    while (!line.isEmpty()) {
        const auto sepIdx = line.indexOf(':');
        results.insert(line.left(sepIdx).toLower(), line.mid(sepIdx + 1).trimmed());

        line = mSocket->readLine().trimmed();
    }

    return results;
}

QByteArray Connection::parseBody(const Headers &headers)
{
    std::optional<int> contentLength;
    if (auto it = headers.find("content-length"); it != headers.cend()) {
        contentLength = it.value().toInt();
    }

    QByteArray buffer;
    buffer.reserve(contentLength.value_or(0));

    static constexpr int chunkSize = 1024;
    int toRead = contentLength.value_or(std::numeric_limits<int>::max());
    while (toRead > 0 && !mSocket->atEnd()) {
        if (mSocket->bytesAvailable() < std::min(toRead, chunkSize)) {
            mSocket->waitForReadyRead();
            continue;
        }

        const auto data = mSocket->read(std::min(toRead, chunkSize));
        toRead -= data.size();

        buffer.append(data);
    }

    return buffer;
}

void Connection::writeResponse(const QByteArray &response)
{
    const Headers responseHeaders = {
        {"content-type", "application/json; charset=utf-8"},
        {"date", QDateTime::currentDateTimeUtc().toString(Qt::ISODate).toUtf8()},
        {"server", "slackserver"}
    };

    QDataStream stream(mSocket.get());

    stream << "HTTP/1.1 200 OK\r\n";
    for (auto it = responseHeaders.constKeyValueBegin(), end = responseHeaders.constKeyValueEnd(); it != end; ++it) {
        const auto &[name, value] = *it;
        stream << name << ": " << value << "\r\n";
    }
    stream << "\r\n"
           << response
           << "\r\n";

    mSocket->flush();
}
