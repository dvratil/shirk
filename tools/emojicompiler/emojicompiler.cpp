// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#include "writer.h"
#include "indexedstringlist.h"
#include "parsedemoji.h"
#include "parsedcategory.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include "utils/stringliterals.h"
#include "utils/compat.h"

#include <range/v3/view/transform.hpp>
#include <range/v3/algorithm/stable_sort.hpp>
#include "utils/ranges.h"

#include <iostream>

#include <QDebug>

using namespace Shirk::StringLiterals;

struct Emoji {
    static Emoji fromJson(const QJsonValue &emoji) {
        const auto short_names = emoji["short_names"_ql1].toArray();
        return {
            .image = emoji["image"_ql1].toString(),
            .short_names = ranges::views::transform(short_names, [](const QJsonValue &v) { return v.toString(); }) | ranges::to_qt<QStringList>(),
            .text = emoji["text"_ql1].toString(),
            .category = emoji["category"_ql1].toString(),
            .sort_order = emoji["sort_order"_ql1].toInt(),
            .x = emoji["sheet_x"_ql1].toInt(),
            .y = emoji["sheet_y"_ql1].toInt()
        };
    }

    QString image;
    QStringList short_names;
    QString text;
    QString category;
    int sort_order;
    int x;
    int y;
};

std::vector<Emoji> parseJson(const QJsonArray &array)
{
    return ranges::views::transform(array, Emoji::fromJson) | ranges::to_vector;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QCommandLineParser parser;
    QCommandLineOption inputFileOpt({u"input"_qs, u"i"_qs}, u"Input file"_qs, u"FILE"_qs);
    QCommandLineOption outputFileOpt({u"output"_qs, u"o"_qs}, u"Output file"_qs, u"FILE"_qs);
    parser.addOptions({inputFileOpt, outputFileOpt});
    parser.addHelpOption();
    parser.process(app);

    if (!parser.isSet(inputFileOpt) || !parser.isSet(outputFileOpt)) {
        std::cerr << "Missing argument!" << std::endl;
        parser.showHelp();
        return -1;
    }

    QFile inputFile{parser.value(inputFileOpt)};
    if (!inputFile.open(QIODevice::ReadOnly)) {
        std::cerr << "Error opening input file: " << qUtf8Printable(inputFile.errorString()) << std::endl;
        return -1;
    }

    QJsonParseError error;
    const QJsonDocument document = QJsonDocument::fromJson(inputFile.readAll(), &error);
    if (error.error) {
        std::cerr << "Error while parsing " << qUtf8Printable(inputFile.fileName()) << ": "
                  << qUtf8Printable(error.errorString()) << std::endl;
        return -2;
    }


    const std::vector<Emoji> emojis = parseJson(document.array());

    IndexedStringList names;
    IndexedStringList categories;
    IndexedStringList images;
    int maxAliases = 0;
    std::vector<ParsedEmoji> parsedEmojis;
    parsedEmojis.reserve(emojis.size());

    for (const Emoji &emoji : emojis) {
        ParsedEmoji parsed;
        parsed.sort_order = emoji.sort_order;

        auto name = emoji.short_names.cbegin();
        parsed.name = names.findOrInsert(*name);
        parsed.name_str = *name;

        for (auto alias = ++name; alias != emoji.short_names.cend(); ++alias) {
            parsed.aliases.push_back(names.findOrInsert(*alias));
        }
        maxAliases = std::max(maxAliases, static_cast<int>(parsed.aliases.size()));

        parsed.category = categories.findOrInsert(emoji.category);
        parsed.category_str = emoji.category;

        Q_ASSERT(!images.contains(emoji.image));
        parsed.image = images.insert(emoji.image);

        parsed.x = emoji.x;
        parsed.y = emoji.y;

        parsedEmojis.emplace_back(std::move(parsed));
    }

    // Sort the emojis by categories to make the lookup table simpler
    ranges::stable_sort(parsedEmojis, [&categories](const auto &lhs, const auto &rhs) {
        const auto findFunc = [](const auto &s) {
            return [s](const auto &val) { return val.first == s.category_str; };
        };
        const auto lhsCat = ranges::find_if(categories, findFunc(lhs));
        const auto rhsCat = ranges::find_if(categories, findFunc(rhs));
        return std::distance(categories.begin(), lhsCat) < std::distance(categories.begin(), rhsCat);
    });

    std::vector<ParsedCategory> parsedCategories;
    parsedCategories.reserve(categories.size());

    for (const auto category : categories) {
        ParsedCategory parsed;

        parsed.name = category.second;
        parsed.name_str = category.first;

        const auto emojiBegin = ranges::find_if(parsedEmojis, [&parsed](const auto &emoji) {
                return emoji.category_str == parsed.name_str;
        });
        const auto emojiEnd = ranges::find_if(emojiBegin, parsedEmojis.end(), [&parsed](const auto &emoji) {
                return emoji.category_str != parsed.name_str;
        });

        parsed.emojis = std::make_pair(std::distance(parsedEmojis.begin(), emojiBegin),
                                       std::distance(emojiBegin, emojiEnd));

        parsedCategories.emplace_back(std::move(parsed));
    }

    Writer writer{parser.value(outputFileOpt)};
    writer.writeHeader();
    writer.writeNameTable(u"s_nameTable", names);
    writer.writeNameTable(u"s_categoryTable", categories);
    writer.writeNameTable(u"s_imageTable", images);

    writer.writeEmojiLookupTable(parsedEmojis, maxAliases);
    writer.writeCategoriesVector(parsedCategories);

    writer.writeFooter();

    return 0;
}

