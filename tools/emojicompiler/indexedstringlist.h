// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QString>

#include <vector>
#include <unordered_map>
#include <utility>

#include "utils/compat.h"

#include <range/v3/algorithm/find_if.hpp>

class IndexedStringList
{
public:
    using Offset = std::size_t;
    using Length = std::size_t;

    bool contains(QStringView str) const
    {
        return find_if(str) != mPos.cend();
    }

    std::pair<Offset, Length> findOrInsert(const QString &str)
    {
        if (auto it = find_if(str); it != mPos.cend()) {
            return it->second;
        }

        return insert(str);
    }

    std::pair<Offset, Length> insert(const QString &str)
    {
        auto start = mData.size();
        for (const QChar c : str) {
            mData.push_back(c.unicode());
        }

        return mPos.emplace_back(std::make_pair(str, std::make_pair(start, str.size()))).second;
    }

    std::pair<Offset, Length> find(QStringView str) const
    {
        if (auto it = find_if(str); it != mPos.cend()) {
            return it->second;
        } else {
            Q_ASSERT(false);
        }
    }

    auto size() const
    {
        return mPos.size();
    }

    const std::vector<char16_t> &data() const
    {
        return mData;
    }

    auto begin() const
    {
        return mPos.begin();
    }

    auto end() const
    {
        return mPos.end();
    }

private:
    template<typename T>
    auto find_if(T str) const -> std::vector<std::pair<QString, std::pair<Offset, Length>>>::const_iterator
    {
        return ranges::find_if(mPos, [str](const auto &val) { return val.first == str; });
    }

    std::vector<char16_t> mData;
    std::vector<std::pair<QString, std::pair<Offset, Length>>> mPos;
};

