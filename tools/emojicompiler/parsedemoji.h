// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

struct ParsedEmoji {
    std::pair<std::size_t, std::size_t> name;
    QString name_str;
    std::vector<std::pair<std::size_t, std::size_t>> aliases;
    std::pair<std::size_t, std::size_t> image;
    std::pair<std::size_t, std::size_t> category;
    QString category_str;
    int sort_order;
    int x;
    int y;
};

