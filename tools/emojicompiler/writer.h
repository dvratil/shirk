// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QStringView>
#include <QTextStream>
#include <QFile>

class IndexedStringList;
struct ParsedEmoji;
struct ParsedCategory;
class QString;

class Writer
{
public:
    Writer(const QString &file);

    void writeHeader();
    void writeFooter();
    void writeNameTable(QStringView name, const IndexedStringList &table);
    void writeEmojiLookupTable(const std::vector<ParsedEmoji> &parsedEmoji, int maxAliasSize);
    void writeCategoriesVector(const std::vector<ParsedCategory> &parsedCategory);

private:
    QFile mOutFile;
    QTextStream mStream;
};
