// SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
//
// SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

#pragma once

#include <span>

struct ParsedCategory
{
    std::pair<std::size_t, std::size_t> name;
    QString name_str;
    std::pair<std::size_t, std::size_t> emojis;
};

